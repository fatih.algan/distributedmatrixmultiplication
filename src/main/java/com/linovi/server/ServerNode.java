package com.linovi.server;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.IHeartbeatProcessor;
import com.linovi.common.transport.ITransportListener;
import com.linovi.common.transport.UDPTransportListener;
import com.linovi.common.transport.UDPTransportSender;

public class ServerNode {

	public static void main(String[] args) {
		LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.FINE);
		if(args.length == 0) throw new RuntimeException("Please specify the port to listen on as first argument - Ex: 110");
		int port = Integer.MIN_VALUE;
		try {
			port = Integer.parseInt(args[0]);
			if(port < 0 || port > 65535) throw new Exception(port + "");
		} catch(Exception e) {
			throw new RuntimeException("Not a valid port value: " + args[0]);
		}
		HostInfo hostInfo = null;
		
		if(args.length == 2) hostInfo = new HostInfo(args[1], port);  
		else hostInfo = new HostInfo("localhost", port);
		
		UDPTransportListener transportListener = new UDPTransportListener(hostInfo.getHost(), hostInfo.getPort());
		UDPTransportSender transportSender = new UDPTransportSender();
		
		IHeartbeatProcessor heartbeatProcessor = new HeartbeatProcessor(transportSender);
		transportListener.addHeartbeatListener(heartbeatProcessor);
		
		PhaseListenerImpl matrixMultiplier = new PhaseListenerImpl(hostInfo, transportSender);
		transportListener.addTransportDataListener(matrixMultiplier);
		
		Thread listenerT = new Thread(transportListener);
		Thread senderT = new Thread(transportSender);
				
		listenerT.start();
		senderT.start();
				
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			matrixMultiplier.shutDown();
			transportListener.shutDown();
			transportSender.shutDown();
		}));
	}
}
