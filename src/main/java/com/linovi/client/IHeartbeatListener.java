package com.linovi.client;

import com.linovi.common.transport.HostInfo;

public interface IHeartbeatListener {

	public void notifyNodeCollapsed(HostInfo hostInfo);
	
}
