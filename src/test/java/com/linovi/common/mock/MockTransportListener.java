package com.linovi.common.mock;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.linovi.common.transport.IHeartbeatProcessor;
import com.linovi.common.transport.ITransportDataProcessor;
import com.linovi.common.transport.ITransportListener;

public class MockTransportListener implements ITransportListener {

	private List<ITransportDataProcessor> dataListeners = new CopyOnWriteArrayList<>();
	private List<IHeartbeatProcessor> heartbeatListeners = new CopyOnWriteArrayList<>();
	
	
	@Override
	public void addTransportDataListener(ITransportDataProcessor listener) {
		dataListeners.add(listener);		
	}

	@Override
	public void removeTransportDataListener(ITransportDataProcessor listener) {
		dataListeners.remove(listener);		
	}

	@Override
	public void addHeartbeatListener(IHeartbeatProcessor listener) {
		heartbeatListeners.add(listener);
		
	}

	@Override
	public void removeHeartbeatListener(IHeartbeatProcessor listener) {
		heartbeatListeners.remove(listener);		
	}

	@Override
	public void shutDown() {
		// TODO Auto-generated method stub
		
	}

}
