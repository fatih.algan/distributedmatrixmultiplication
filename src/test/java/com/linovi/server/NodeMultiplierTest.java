package com.linovi.server;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialFirstMatrixData;

import junit.framework.Assert;

public class NodeMultiplierTest {

	MockTransportDataSender transportSender;
	NodeMultiplier nodeMultiplier;
	
	HostInfo hostInfo;
	HostInfo localhostInfo;
	int nodeId;
	
	int[][] firstMatrix = new int[][]{{8, 0, 8, 6, 3, 3}, {6, 2, 7, 4, 6, 6}, {8, 9, 5, 6, 0, 0}, {8, 2, 9, 1, 7, 5}, {5, 2, 3, 9, 1, 5}};;
	int[][] secondMatrix = new int[][] {{0, 2, 5, 2, 9, 5, 4}, {6, 6, 1, 6, 3, 9, 8}, {4, 3, 4, 4, 4, 5, 7}, {7, 4, 5, 4, 4, 8, 8}, {3, 4, 7, 0, 5, 3, 1}, {7, 3, 6, 2, 3, 7, 1}};
	
	@Before
	public void setUp() {
		hostInfo = new HostInfo("192.168.0.12", 1141);
		localhostInfo = new HostInfo("localhost", 1111);
		transportSender = new MockTransportDataSender();
		nodeMultiplier = new NodeMultiplier(localhostInfo, nodeId, 6, 6, 7, transportSender);
	}
	
	@Test
	public void testDistributeToPartition() {
		PartialFirstMatrixData data = new PartialFirstMatrixData(0, 0, 2, 1, 2, 5, new int[] {7,  4,  6, 6}, hostInfo.getHost(), hostInfo.getPort(), 111);
		PartialFirstMatrixAckData resp = nodeMultiplier.distributeToPartition(data);
		Assert.assertEquals(resp.getSequence(), 2);
		Assert.assertEquals(resp.getPartitionId(), 0);
	}
}
