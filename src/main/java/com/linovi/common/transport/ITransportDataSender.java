package com.linovi.common.transport;

public interface ITransportDataSender {

	public void sendTransportDataMessage(ITransportDataMessage data);
	public void sendHeartbeatMessage(ITransportMessage data);
	public void shutDown();
		
}
