package com.linovi.common.transport;

import java.net.InetAddress;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PartialResultAckDataTest {

	int sequence = 27;
	int partitionId = 2;
	int nodeId = 1;
	int port = 80;
	int listenPort = 110;
	
	byte[] encoded = null;
	PartialResultAckData msg;
	
	@Before
	public void setUp() {
		try {
			msg = new PartialResultAckData(nodeId, partitionId, sequence, InetAddress.getLocalHost(), 
				port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			PartialResultAckData decoded = (PartialResultAckData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getSequence(), msg.getSequence());
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getPartitionId(), msg.getPartitionId());
			Assert.assertEquals(decoded.getNodeIndex(), msg.getNodeIndex());
		} catch(Exception e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
