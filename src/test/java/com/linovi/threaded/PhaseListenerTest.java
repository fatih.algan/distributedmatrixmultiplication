package com.linovi.threaded;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.InitData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialResultAckData;
import com.linovi.common.transport.PartialSecondMatrixData;
import com.linovi.common.transport.RequestOperationResultData;
import com.linovi.server.MultiplierEngine;
import com.linovi.server.PartitionMultiplier;
import com.linovi.server.PhaseListenerImpl;

import org.junit.Assert;

public class PhaseListenerTest {

	PhaseListenerImpl phaseListener;
	MockTransportDataSender transportSender;
	
	HostInfo localHostInfo;
	HostInfo host;
	
	int[][] firstMatrix = new int[][]{{8, 0, 8, 6, 3, 3}, {6, 2, 7, 4, 6, 6}, {8, 9, 5, 6, 0, 0}, {8, 2, 9, 1, 7, 5}, {5, 2, 3, 9, 1, 5}};
	int[][] secondMatrix = new int[][] {{0, 2, 5, 2, 9, 5, 4}, {6, 6, 1, 6, 3, 9, 8}, {4, 3, 4, 4, 4, 5, 7}, {7, 4, 5, 4, 4, 8, 8}, {3, 4, 7, 0, 5, 3, 1}, {7, 3, 6, 2, 3, 7, 1}};
	int[][] result = new int[firstMatrix.length][secondMatrix[0].length];
	
	@Before
	public void setUp() {
		host = new HostInfo("192.168.0.12", 1131);
		localHostInfo = new HostInfo("192.168.0.16", 1131);
		transportSender = new MockTransportDataSender();
		phaseListener = new PhaseListenerImpl(localHostInfo, transportSender);
		InitData init = new InitData(0, host.getHost(), host.getPort(), 1141, 5, 6, 6, 7);
		phaseListener.dataReceived(init);
		PartialSecondMatrixData sec = new PartialSecondMatrixData(0, 0, 0, 0, new int[] {0, 2, 5, 2}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 1, 0, 4, new int[] {9, 5, 4, 6}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 2, 1, 1, new int[] {6, 1, 6, 3}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 3, 1, 5, new int[] {9, 8, 4, 3}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 4, 2, 2, new int[] {4, 4, 4, 5}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 5, 2, 6, new int[] {7, 7, 4, 5}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 6, 3, 3, new int[] {4, 4, 8, 8}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 7, 4, 0, new int[] {3, 4, 7, 0}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 8, 4, 4, new int[] {5, 3, 1, 7}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 9, 5, 1, new int[] {3, 6, 2, 3}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		sec = new PartialSecondMatrixData(0, 10, 5, 5, new int[] {7, 1}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);
		PartialFirstMatrixData sec2 = new PartialFirstMatrixData(0, 0, 0, 0, 0, 5, new int[] {8, 0, 8, 6}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 1, 0, 4, 5, new int[] {3, 3, 6, 2}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 2, 1, 2, 5, new int[] {7, 4, 6, 6}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 3, 2, 0, 5, new int[] {8, 9, 5, 6}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 4, 2, 4, 5, new int[] {0, 0, 8, 2}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 5, 3, 2, 5, new int[] {9, 1, 7, 5}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 6, 4, 0, 5, new int[] {5, 2, 3, 9}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		sec2 = new PartialFirstMatrixData(0, 0, 7, 4, 4, 5, new int[] {1, 5}, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec2);
		RequestOperationResultData sec3 = new RequestOperationResultData(0, 0, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec3);
		try {
			Thread.sleep(4000);
		} catch(Exception e) {}
		sendAcks();
	}
	
	@Test
	public void testMultiplication() {
		PartitionMultiplier mult = phaseListener.getNodeMultiplier(0).getPartitionMultiplier(0);
		Assert.assertEquals(mult.getCompletedSequencesPercentage(), 100);
		MultiplierEngine engine = new MultiplierEngine(firstMatrix, secondMatrix, result);
		sendAcks();
		engine.multiply();
		try {
			Thread.sleep(4000);
		} catch(Exception e) {}
		Arrays.toString(mult.getResultMatrix());
		Assert.assertArrayEquals(result, mult.getResultMatrix());
		sendAcks();
	}
	
	public void sendAcks() {
		PartialResultAckData sec = new PartialResultAckData(0, 0, 0, host.getHost(), host.getPort(), 1141);
		phaseListener.dataReceived(sec);		
	}
}
