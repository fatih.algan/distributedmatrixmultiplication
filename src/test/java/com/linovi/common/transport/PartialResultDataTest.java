package com.linovi.common.transport;

import java.net.InetAddress;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PartialResultDataTest {

	int sequence = 49;
	int[] elements = new int[] {1, 3, 5, 8};
	int nodeIndex = 1;
	int partitionId = 1;
	int totalSequenceCount = 5;
	int port = 80;
	int listenPort = 110;
	
	byte[] encoded = null;
	PartialResultData msg;
	
	@Before
	public void setUp() {
		try {
			msg = new PartialResultData(nodeIndex, partitionId, sequence, totalSequenceCount, 
				elements, InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			PartialResultData decoded = (PartialResultData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getSequence(), msg.getSequence());
			Assert.assertEquals(decoded.getTotalSequenceCount(), msg.getTotalSequenceCount());
			Assert.assertEquals(decoded.getPartitionNo(), msg.getPartitionNo());
			Assert.assertArrayEquals(decoded.getElements(), msg.getElements());
		} catch(Exception e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
