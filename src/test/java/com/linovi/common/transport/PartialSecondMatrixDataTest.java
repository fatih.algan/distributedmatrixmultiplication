package com.linovi.common.transport;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PartialSecondMatrixDataTest {

	int sequence = 27;
	
	int[] rowElements = new int[] {1, 3, 5, 7};
	int nodeIndex = 1;
	int rowIndex = 3;
	int rowOffset = 24;
	int port = 80;
	int listenPort = 110;
	
	byte[] encoded = null;
	PartialSecondMatrixData msg;
	
	@Before
	public void setUp() {
		try {
			msg = new PartialSecondMatrixData(nodeIndex, sequence, rowIndex, rowOffset, rowElements, InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			PartialSecondMatrixData decoded = (PartialSecondMatrixData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getNodeIndex(), msg.getNodeIndex());
			Assert.assertEquals(decoded.getSequence(), msg.getSequence());
			Assert.assertArrayEquals(decoded.getRowElements(), msg.getRowElements());
			Assert.assertEquals(decoded.getRowIndex(), msg.getRowIndex());
			Assert.assertEquals(decoded.getRowOffset(), msg.getRowOffset());
		} catch(UnknownHostException e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
