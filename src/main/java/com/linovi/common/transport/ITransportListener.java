package com.linovi.common.transport;

public interface ITransportListener {

	public void addTransportDataListener(ITransportDataProcessor listener);
	public void removeTransportDataListener(ITransportDataProcessor listener);
	public void addHeartbeatListener(IHeartbeatProcessor listener);
	public void removeHeartbeatListener(IHeartbeatProcessor listener);
	public void shutDown();
}
