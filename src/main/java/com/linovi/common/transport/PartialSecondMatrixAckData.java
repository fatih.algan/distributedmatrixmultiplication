package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class PartialSecondMatrixAckData extends PartialMatrixAckData {

	public static final int HEADER_LENGTH = 16;
	
	public PartialSecondMatrixAckData(int nodeIndex, int sequence, InetAddress host, int port, 
		int listenPort) {
		super(nodeIndex, sequence, host, port, listenPort);
	}
	
	protected static PartialSecondMatrixAckData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 4) throw new RuntimeException("This message is not a PartialSecondMatrixAckData message: " + cmd);
		int nodeIndex = buf.getInt();
		int sequence = buf.getInt();
		int listenPort = buf.getInt();
		PartialSecondMatrixAckData data = new PartialSecondMatrixAckData(nodeIndex, sequence, host, port, listenPort);
		return data;
	}
}
