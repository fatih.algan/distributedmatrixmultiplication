package com.linovi.server;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.client.IHeartbeatListener;
import com.linovi.common.transport.HeartbeatMessage;
import com.linovi.common.transport.IHeartbeatProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.ITransportMessage;

public class HeartbeatProcessor implements IHeartbeatProcessor {

	private static final Logger log = Logger.getGlobal();
	
	private ITransportDataSender transportDataSender;
	
	private List<IHeartbeatListener> heartbeatListeners = new ArrayList<>();
	
	public HeartbeatProcessor(ITransportDataSender transportDataSender) {
		this.transportDataSender = transportDataSender;
	}
	
	@Override
	public void heartbeatReceived(ITransportMessage data) throws InterruptedException {
		HeartbeatMessage incoming = (HeartbeatMessage)data;
		HeartbeatMessage responseMsg = new HeartbeatMessage(incoming.getHost(), incoming.getReplyHost(), data.getListenPort(), data.getPort());
		log.log(Level.FINE, "Sending heartbeat response: " + responseMsg.toString());
		transportDataSender.sendHeartbeatMessage(responseMsg);
	}

	@Override
	public void addHeartbeatListener(IHeartbeatListener listener) {
		heartbeatListeners.add(listener);
	}

	@Override
	public void removeHeartbeatListener(IHeartbeatListener listener) {
		heartbeatListeners.remove(listener);		
	}
}
