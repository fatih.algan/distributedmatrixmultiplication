package com.linovi.server;

import org.junit.Assert;
import org.junit.Test;

public class MultiplierEngineTest {

	int[][] a = new int[][] {{1, 2, 3,}, {4, 5, 6}};
	int[][] b = new int[][] {{7, 1}, {8, 2}, {9, 3}};
	int[][] result = new int[a.length][b[0].length];
	
	int[][] testResult = new int[][] {{50, 14}, {122, 32}};
	
    @Test
    public void testMultiply() {
    	IMultiplierEngine engine = new MultiplierEngine(a, b, result);
    	engine.multiply();
    	Assert.assertArrayEquals(testResult, result);
    }
}
