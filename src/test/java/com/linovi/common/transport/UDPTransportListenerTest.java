package com.linovi.common.transport;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class UDPTransportListenerTest {

	private UDPTransportListener transportListener = new UDPTransportListener(1111);
	
	private MockTransportDataReceiver transportReceiver = new MockTransportDataReceiver();
	private MockHeartbeatProcessor heartbeatReceiver = new MockHeartbeatProcessor();
	
	private InitData dataMessage = null;
	private HeartbeatMessage heartbeatMessage = null;
	
	@Before
	public void setUp() {
		transportListener.addHeartbeatListener(heartbeatReceiver);
		transportListener.addTransportDataListener(transportReceiver);
		Thread listenerThread = new Thread(transportListener);
		listenerThread.start();
		//Allow transport listener to initialize its socket by waiting a little
		delay();
		DatagramSocket socket = null;
		try {
			dataMessage = new InitData(0, InetAddress.getLocalHost(), 1111, 1101, 100, 150, 150, 74);
			heartbeatMessage = new HeartbeatMessage(InetAddress.getLocalHost(), InetAddress.getLocalHost(), 1111, 1101);
			socket = new DatagramSocket();
			byte[] msg1 = dataMessage.encode();
			DatagramPacket request1 = new DatagramPacket(msg1, msg1.length, dataMessage.getHost(), dataMessage.getPort());
			byte[] msg2 = heartbeatMessage.encode();
			DatagramPacket request2 = new DatagramPacket(msg2, msg2.length, heartbeatMessage.getHost(), heartbeatMessage.getPort());
			socket.send(request1);
			socket.send(request2);
		} catch(Exception e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			socket.close();
		}
		delay();
		transportListener.shutDown();
	}
	
	private void delay() {
		try {
			Thread.sleep(100);
		} catch(Exception e) {
			
		}
	}
	
	@Test
	public void testListenMessages() {
		Assert.assertTrue(transportReceiver.isDataReceived());
		Assert.assertTrue(heartbeatReceiver.isHeartbeatReceived());
	}
	
}
