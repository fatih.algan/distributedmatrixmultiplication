package com.linovi.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportDataProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.PartialMatrixData;
import com.linovi.common.transport.PartialSecondMatrixAckData;
import com.linovi.common.transport.PartialSecondMatrixData;
import com.linovi.common.transport.TransportParams;

public class SendWholeMatrixManager implements Runnable, ITransportDataProcessor {

	private static final Logger log = Logger.getGlobal();
	
	private ITransportDataSender transportSender;
	
	private final int nodeId;
	
	private final MatrixManager matrixManager;
	
	private HashMap<HostInfo, Boolean[]> phaseProgress = new HashMap<>();
		
	public SendWholeMatrixManager(List<HostInfo> hosts, ITransportDataSender transportSender, int nodeIndex,
		int[][] wholeMatrix) {
		this.nodeId = nodeIndex;
		this.transportSender = transportSender;
		this.matrixManager = new MatrixManager(wholeMatrix);
		IntStream.range(0, hosts.size()).forEach(i -> {
			Boolean[] sequences = new Boolean[matrixManager.getTotalPackets()];
			IntStream.range(0, matrixManager.getTotalPackets()).forEach(k -> sequences[k] = new Boolean(false));
			phaseProgress.put(hosts.get(i), sequences);
		});
		log.log(Level.FINE, "Total sequence count is: " + matrixManager.getTotalPackets());
	}

	public boolean phaseComplete() {
		Set<HostInfo> hosts = phaseProgress.keySet();
		for(HostInfo host : hosts) {
			Boolean[] sequences = phaseProgress.get(host);
			if(sequences == null) continue;
			return Arrays.stream(sequences).allMatch(i -> i);
		}
		return true;
	}
	
	@Override
	public void dataReceived(ITransportDataMessage msg) {
		if(nodeId != msg.getNodeIndex()) return;
		log.log(Level.FINE, msg.toString());
		if(msg instanceof PartialSecondMatrixAckData) {
			HostInfo hostInfo = new HostInfo(msg.getHost(), msg.getListenPort());
			Boolean[] sequences = phaseProgress.get(hostInfo);
			sequences[msg.getSequence()] = true;
		}
	}
	
	//When this thread completes all alive nodes will have received the complete second matrix
	//and sent their acks
	@Override
	public void run() {
		boolean keepRunning = true;
		boolean sequencesComplete = false;
		while(!sequencesComplete && keepRunning) {
			log.log(Level.FINE, "Checking whether second matrix has been completely distributed.");
			sequencesComplete = true;
			try {
				for(int i = 0; i < matrixManager.getTotalPackets(); i++) {
					if(!distributeSequence(i)) sequencesComplete = false;
					if(i % 5000 == 0) Thread.sleep(500);
				}
				Thread.sleep(2000);
			} catch(InterruptedException ie) {
				keepRunning = false;
			}
		}
		log.log(Level.INFO, "SendWholeMatrixManager is quitting..");
	}
	
	private boolean distributeSequence(int sequenceNo) throws InterruptedException {
		boolean sequenceComplete = true;
		int rowIndex = matrixManager.getBeginningRowIndexOfSequence(sequenceNo);
		int rowOffset = matrixManager.getBeginningRowOffsetOfSequence(sequenceNo);
		int[] partialMatrix = matrixManager.getPartialRowOfSequence(sequenceNo);
		Set<HostInfo> hosts = phaseProgress.keySet();
		for(HostInfo info : hosts) {
			if(!phaseProgress.get(info)[sequenceNo]) {
				sequenceComplete = false;
				PartialMatrixData msg = new PartialSecondMatrixData(nodeId, sequenceNo, rowIndex,
						rowOffset, partialMatrix, info.getHost(), info.getPort(), TransportParams.CLIENT_LISTEN_PORT);
				transportSender.sendTransportDataMessage(msg);
			}
		}
		return sequenceComplete;
	}
}
