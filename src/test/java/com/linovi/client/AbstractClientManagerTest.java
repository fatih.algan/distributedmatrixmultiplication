package com.linovi.client;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataMessage;

public class AbstractClientManagerTest {

	protected MockTransportDataSender transportSender;
	protected List<HostInfo> hosts = new ArrayList<>();
	
	protected HostInfo host1;
	protected HostInfo host2;
	
	public List<ITransportDataMessage> removeDuplicates(List<ITransportDataMessage> messages) {
		Set<ITransportDataMessage> queued = new HashSet<>(messages);
		return new ArrayList<>(queued);
	}
		
	
}
