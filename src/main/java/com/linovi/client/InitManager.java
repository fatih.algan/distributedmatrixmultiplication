package com.linovi.client;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportDataProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.InitAckData;
import com.linovi.common.transport.InitData;
import com.linovi.common.transport.TransportParams;

public class InitManager implements ITransportDataProcessor, Runnable {

	private static final Logger log = Logger.getGlobal();
	
	private ITransportDataSender transportSender;
	
	private volatile boolean shutDown = false;
	private volatile HashMap<HostInfo, Boolean> phaseProgress = new HashMap<>();
	private final int nodeIndex;
	private final int firstMatrixRowCount, firstMatrixColumnCount;
	private final int secondMatrixRowCount, secondMatrixColumnCount;
	
	public InitManager(List<HostInfo> hosts, ITransportDataSender transportSender, int nodeIndex, 
		int firstRowCount, int firstColumnCount, int secondRowCount, int secondColumnCount) {
		IntStream.range(0, hosts.size()).forEach(i -> phaseProgress.put(hosts.get(i), new Boolean(false)));
		this.transportSender = transportSender;
		this.nodeIndex = nodeIndex;
		this.firstMatrixColumnCount = firstColumnCount;
		this.firstMatrixRowCount = firstRowCount;
		this.secondMatrixColumnCount = secondColumnCount;
		this.secondMatrixRowCount = secondRowCount;
	}
	
	public void removeHost(HostInfo hostInfo) {
		phaseProgress.remove(hostInfo);		
	}


	//When this thread completes the server will have received its init data
	// and sent ack
	@Override
	public void run() {
		//send init messages to all servers and then wait/loop until all acks are received
		sendInitMessages();
		while(!shutDown) {
			shutDown = true;
			phaseProgress.forEach((k, v) -> {if(!v) shutDown = false;});
			try {
				Thread.sleep(3000);
			} catch(InterruptedException ie) {
				shutDown = true;
			}
		}
		log.log(Level.INFO, "Init Manager is quiting..");
	}
	
	//Send init message to server nodes which will initialize second matrix
	private void sendInitMessages() {
		phaseProgress.forEach((k, v) -> {
			InitData msg = new InitData(nodeIndex, k.getHost(), k.getPort(), TransportParams.CLIENT_LISTEN_PORT, 
				firstMatrixRowCount, firstMatrixColumnCount, secondMatrixRowCount, secondMatrixColumnCount);
			transportSender.sendTransportDataMessage(msg);
		});
	}

	@Override
	public void dataReceived(ITransportDataMessage msg) {
		if(nodeIndex != msg.getNodeIndex()) return;
		if(msg instanceof InitAckData) {
			log.log(Level.INFO, msg.toString());
			HostInfo hostInfo = new HostInfo(msg.getHost(), msg.getListenPort());
			phaseProgress.put(hostInfo, true);
		}
	}
	
	public boolean isPhaseComplete() {
		return phaseProgress.entrySet().stream().allMatch(k -> k.getValue()); 
	}
}
