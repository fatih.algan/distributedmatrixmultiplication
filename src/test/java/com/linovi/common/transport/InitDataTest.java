package com.linovi.common.transport;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class InitDataTest {

	int sequence = 1;
	int nodeIndex = 0;
	int port = 110;
	int listenPort = 80;
	
	int firstMatrixRowLength = 5, firstMatrixColumnLength = 3;
	int secondMatrixRowLength = 3, secondMatrixColumnLength = 4;
	
	InitData msg = null;
	
	byte[] encoded = null;
	
	@Before
	public void setUp() {
		try {
			msg = new InitData(nodeIndex, InetAddress.getLocalHost(), port, listenPort, firstMatrixRowLength, firstMatrixColumnLength, secondMatrixRowLength, secondMatrixColumnLength);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			InitData decoded = (InitData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), listenPort);
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getFirstMatrixRowLength(), msg.getFirstMatrixRowLength());
			Assert.assertEquals(decoded.getSecondMatrixColumnLength(), msg.getSecondMatrixColumnLength());
		} catch(UnknownHostException e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
