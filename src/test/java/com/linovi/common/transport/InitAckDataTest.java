package com.linovi.common.transport;

import java.net.InetAddress;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class InitAckDataTest {

	int listenPort = 80;
	int port = 110;
	byte[] encoded = null;
	
	InitAckData msg = null;
	
	@Before
	public void setUp() {
		try {
			msg = new InitAckData(0, InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			InitAckData decoded = (InitAckData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
		} catch(Exception e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
	
	
}
