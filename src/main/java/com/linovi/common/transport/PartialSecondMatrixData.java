package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class PartialSecondMatrixData extends PartialMatrixData {

	public static final int HEADER_LENGTH = 24;
	
	public PartialSecondMatrixData(int nodeIndex, int sequence, int rowIndex, int rowOffset, 
		int[] rowElements,InetAddress host, int port, int listenPort) {
		super(nodeIndex, sequence, rowIndex, rowOffset, 
			rowElements, host, port, listenPort);
	}
	
	protected static PartialSecondMatrixData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 3) throw new RuntimeException("This message is not a PartialSecondMatrixData message: " + cmd);
		int nodeIndex = buf.getInt();
		int sequence = buf.getInt();
		int listenPort = buf.getInt();
		int rowIndex = buf.getInt();
		int rowOffset = buf.getInt();
		int[] rowElements = new int[(message.length - HEADER_LENGTH) / 4];
		for(int i = 0; i < rowElements.length; i++) {
			rowElements[i] = buf.getInt();
		}
		PartialSecondMatrixData data = new PartialSecondMatrixData(nodeIndex, sequence, rowIndex, rowOffset, rowElements, 
			host, port, listenPort);
		return data;
	}
	
	
}
