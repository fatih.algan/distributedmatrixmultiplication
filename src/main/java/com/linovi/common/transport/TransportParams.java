package com.linovi.common.transport;

public class TransportParams {

	public static int MAX_PACKET_SIZE = 507;
	
	public static final int CLIENT_LISTEN_PORT = 1141;
	
	public static void setMaxPacketSize(int maxPacketSize) {
		if(maxPacketSize < 0 || maxPacketSize > 65507) return;
		TransportParams.MAX_PACKET_SIZE = maxPacketSize;
	}
}
