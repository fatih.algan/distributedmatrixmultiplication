package com.linovi.client;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.common.transport.HostInfo;

public class PartitionInfo implements Serializable, Comparable<PartitionInfo> {

	private static final long serialVersionUID = -5916175334350878684L;
	
	private static final Logger log = Logger.getGlobal();
	
	private final int partitionId;
	private final int startRowIndex;
	private final int endRowIndex;
	private final int resultColumnCount;
	private volatile int[][] resultMatrix;
	
	private final HostInfo hostInfo;
	
	private AtomicBoolean[] sequences = null;
	private volatile int arrivedSequencesCount = 0;
	
	public PartitionInfo(HostInfo hostInfo, int partitionId, int startRowIndex, int endRowIndex,
		int resultColumnCount) {
		this.partitionId = partitionId;
		this.startRowIndex = startRowIndex;
		this.endRowIndex = endRowIndex;
		this.hostInfo = hostInfo;
		this.resultColumnCount = resultColumnCount;
		resultMatrix = new int[endRowIndex - startRowIndex + 1][resultColumnCount];
	}

	public int getPartitionId() {
		return partitionId;
	}

	public int getStartRowIndex() {
		return startRowIndex;
	}

	public int getEndRowIndex() {
		return endRowIndex;
	}
	
	public HostInfo getHostInfo() {
		return hostInfo;
	}
	
	public int[][] getResultMatrix() {
		return resultMatrix;
	}
	
	public boolean isAllSequencesComplete() {
		if(sequences == null) return false;
		for(int i = 0; i < sequences.length; i++) {
			if(sequences[i] == null) return false;
			if(!sequences[i].get()) return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + partitionId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartitionInfo other = (PartitionInfo) obj;
		if (partitionId != other.partitionId)
			return false;
		return true;
	}
	
	public void writePartialResult(int[] elements, int sequence, int totalSequenceCount) {
		try {
			if(sequences == null) {
				sequences = new AtomicBoolean[totalSequenceCount];
				for(int i = 0; i < sequences.length; i++) sequences[i] = new AtomicBoolean(false);
			}
			if(sequences[sequence].compareAndSet(false, true)) {
				int totalElementsInPartition = (endRowIndex - startRowIndex + 1) * resultColumnCount;
				int beginElementOrder = 0;
				if(sequence != totalSequenceCount - 1) beginElementOrder = elements.length * sequence;
				else beginElementOrder = totalElementsInPartition - elements.length;
				int beginRowIndex = beginElementOrder / resultMatrix[0].length;
				int beginColumnIndex = (beginElementOrder % resultMatrix[0].length);
				for(int i = 0; i < elements.length; i++) {
					resultMatrix[beginRowIndex][beginColumnIndex] = elements[i];
					if(beginColumnIndex == resultMatrix[0].length - 1) {
						beginColumnIndex = 0;
						beginRowIndex++;
					} else {
						beginColumnIndex++;
					}
				}
				arrivedSequencesCount++;
			}
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error while writing partial results:" + e.getMessage());
		}
	}

	@Override
	public int compareTo(PartitionInfo o) {
		return new Integer(getStartRowIndex()).compareTo(o.getStartRowIndex());
	}
	
	public int getCompletedSequencesPercentage() {
		if(sequences == null) return 0;
		return (arrivedSequencesCount / sequences.length) * 100;
	}
	
}
