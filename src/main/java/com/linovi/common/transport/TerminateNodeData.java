package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class TerminateNodeData extends InitAckData {
	
	public static final int HEADER_LENGTH = 12;
	
	public TerminateNodeData(int nodeIndex, InetAddress host, int port, int listenPort) {
		super(nodeIndex, host, port, listenPort);
	}
	
	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH + 4);
		buf.putInt(10);
		buf.putInt(getNodeIndex());
		buf.putInt(getListenPort());
		return buf.array();
	}
	
	protected static TerminateNodeData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 10) throw new RuntimeException("This message is not a Terminate Node message: " + cmd);
		int nodeIndex = buf.getInt();
		int listenPort = buf.getInt();
		TerminateNodeData data = new TerminateNodeData(nodeIndex, host, port, listenPort);
		return data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TerminateNode from [" + getHost().getHostAddress() + ":" + getListenPort() + "] ");
		builder.append("[node:" + getNodeIndex());
		builder.append("]");
		return builder.toString();
	}
	
}
