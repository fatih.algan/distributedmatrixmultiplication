package com.linovi.common.transport;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class UDPTransportSenderTest {

	private ITransportMessage testMessage;
	
	private DatagramSocket socket;
				
	@Before
	public void setUp() {
		try {
			testMessage = new HeartbeatMessage(InetAddress.getLocalHost(), InetAddress.getLocalHost(), 1111, 1111);
		} catch(Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	private ITransportMessage decodeMessage(byte[] message, InetAddress host, int port) {
		return ITransportMessage.decode(message, host, port);
	}
	
	@Test
	public void testSendMessage() {
		UDPTransportSender sender = new UDPTransportSender();
		Thread starter = new Thread(() -> {
			try {
				Thread.sleep(1000);
				sender.sendHeartbeatMessage(testMessage);
				Thread t = new Thread(sender);
				t.start();
			} catch(Exception e) {
				
			}
		});
		starter.start();
		ITransportMessage result = null;
		try {
			socket = new DatagramSocket(1111);
			DatagramPacket request = new DatagramPacket(new byte[TransportParams.MAX_PACKET_SIZE], TransportParams.MAX_PACKET_SIZE);
			socket.receive(request);
			byte[] message = new byte[request.getLength()];
			System.arraycopy(request.getData(), 0, message, 0, message.length);
			result = decodeMessage(message, request.getAddress(), request.getPort());
			socket.close();
		} catch(Exception e) {
			socket.close();
			throw new RuntimeException(e.getMessage());
		}
		Assert.assertEquals(testMessage.getListenPort(), result.getListenPort());
		socket.close();
	}
}
