package com.linovi.common.mock;

import com.linovi.client.IPartialResultObserver;
import com.linovi.client.PartitionInfo;

public class MockPartialResultObserver implements IPartialResultObserver {

	boolean partitionComplete;
	PartitionInfo partitionInfo;
	
	@Override
	public void notifyPartitionComplete(PartitionInfo partitionInfo) {
		this.partitionInfo = partitionInfo;
		partitionComplete = true;
	}

	public boolean isPartitionComplete() {
		return partitionComplete;
	}

	public PartitionInfo getPartitionInfo() {
		return partitionInfo;
	}
	

}
