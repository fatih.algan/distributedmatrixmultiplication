package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class InitData implements ITransportDataMessage {

	private final int sequence;
	private final int nodeIndex;
	private final InetAddress host;
	private final int port;
	private final int listenPort;
	
	private final int firstMatrixRowLength, firstMatrixColumnLength, secondMatrixRowLength, secondMatrixColumnLength; 
	
	public static final int HEADER_LENGTH = 32;
	
	public InitData(int nodeIndex, InetAddress host, int port, int listenPort, int firstMatrixRowLength,
		int firstMatrixColumnLength, int secondMatrixRowLength, int secondMatrixColumnLength) {
		this.sequence = 0;
		this.nodeIndex = nodeIndex;
		this.host = host;
		this.port = port;
		this.listenPort = listenPort;
		this.firstMatrixRowLength = firstMatrixRowLength;
		this.firstMatrixColumnLength = firstMatrixColumnLength;
		this.secondMatrixColumnLength = secondMatrixColumnLength;
		this.secondMatrixRowLength = secondMatrixRowLength;
	}
	
	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}
	
	public int getSequence() {
		return sequence;
	}
	
	public int getNodeIndex() {
		return nodeIndex;
	}
	
	public int getFirstMatrixRowLength() {
		return firstMatrixRowLength;
	}

	public int getFirstMatrixColumnLength() {
		return firstMatrixColumnLength;
	}

	public int getSecondMatrixRowLength() {
		return secondMatrixRowLength;
	}

	public int getSecondMatrixColumnLength() {
		return secondMatrixColumnLength;
	}

	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH);
		buf.putInt(1);
		buf.putInt(nodeIndex);
		buf.putInt(listenPort);
		buf.putInt(firstMatrixRowLength);
		buf.putInt(firstMatrixColumnLength);
		buf.putInt(secondMatrixRowLength);
		buf.putInt(secondMatrixColumnLength);
		return buf.array();
	}
	
	protected static InitData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 1) throw new RuntimeException("This message is not an Init message: " + cmd);
		int nodeIndex = buf.getInt();
		int listenPort = buf.getInt();
		int firstMatrixRowLength = buf.getInt();
		int firstMatrixColumnLength = buf.getInt();
		int secondMatrixRowLength = buf.getInt();
		int secondMatrixColumnLength = buf.getInt();
		InitData data = new InitData(nodeIndex, host, port, listenPort, firstMatrixRowLength, 
			firstMatrixColumnLength, secondMatrixRowLength, secondMatrixColumnLength);
		return data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Init from [" + host.getHostAddress() + ":" + listenPort + "] ");
		builder.append("[node:" + nodeIndex + " - seq:" +  sequence + " - " + 
		" - firstRowLength:" + firstMatrixRowLength + " - firstColumnLength:" + firstMatrixColumnLength + 
		" - secondRowLength:" + secondMatrixRowLength + " - secondColumnLength:" + secondMatrixColumnLength);
		builder.append("]");
		return builder.toString();
	}
}
