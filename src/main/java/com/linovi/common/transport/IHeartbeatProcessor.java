package com.linovi.common.transport;

import com.linovi.client.IHeartbeatListener;

public interface IHeartbeatProcessor {

	public void heartbeatReceived(ITransportMessage data) throws InterruptedException;
	public void addHeartbeatListener(IHeartbeatListener listener);
	public void removeHeartbeatListener(IHeartbeatListener listener);
	
}
