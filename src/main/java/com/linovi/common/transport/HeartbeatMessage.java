package com.linovi.common.transport;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class HeartbeatMessage implements ITransportMessage {

	private final InetAddress host;
	private final InetAddress replyHost;
	private final int port;
	private final int listenPort;
	
	public static final int HEADER_LENGTH = 8;
	
	public HeartbeatMessage(InetAddress host, InetAddress replyHost, int port, int listenPort) {
		this.host = host;
		this.replyHost = replyHost;
		this.port = port;
		this.listenPort = listenPort;
	}
	
	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}
	
	public InetAddress getReplyHost() {
		return replyHost;
	}

	public byte[] encode() {
		String strHost = replyHost.getHostAddress();
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH + replyHost.getAddress().length);
		buf.putInt(0);
		buf.putInt(listenPort);
		buf.put(replyHost.getAddress());
		return buf.array();
	}
	
	protected static HeartbeatMessage decode(byte[] message, InetAddress host, int port) {
		try {
			ByteBuffer buf = ByteBuffer.wrap(message);
			int cmd = buf.getInt();
			if(cmd != 0) throw new RuntimeException("This message is not a Heartbeat message: " + cmd);
			int listenPort = buf.getInt();
			byte[] replyHostBytes = new byte[(message.length - HEADER_LENGTH)];
			buf.get(replyHostBytes);
			InetAddress replyHost = InetAddress.getByAddress(replyHostBytes);
			//String fullHostName = replyHost.getHostAddress();
			//String[] divs = fullHostName.split("/");
			//String hostName = divs[0];
			HeartbeatMessage data = new HeartbeatMessage(host, replyHost, port, listenPort);
			return data;
		} catch(UnknownHostException e) {
			throw new RuntimeException("Unknown host:" + e.getMessage());
		}
	}
	
	@Override
	public String toString() {
		return "Heartbeat from " + "[" + host.getHostAddress() + ":" + port + "]";
	}
}
