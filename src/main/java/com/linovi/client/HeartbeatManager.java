package com.linovi.client;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.common.transport.HeartbeatMessage;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.IHeartbeatProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.ITransportMessage;
import com.linovi.common.transport.TransportParams;

public class HeartbeatManager implements IHeartbeatProcessor, Runnable {

	private static final Logger log = Logger.getGlobal();
	
	private ITransportDataSender transportSender;
	private List<HostInfo> hosts = null;
		
	private AtomicInteger nextHost = new AtomicInteger(0);
		
	private volatile boolean heartBeatReceived = false;
	
	private volatile boolean shutDown = false;
	
	private List<IHeartbeatListener> heartbeatListeners = new ArrayList<>();
	
	public HeartbeatManager(ITransportDataSender transportSender, List<HostInfo> hosts) {
		this.transportSender = transportSender;
		this.hosts = hosts;
	}
	
	public void addHeartbeatListener(IHeartbeatListener listener) {
		heartbeatListeners.add(listener);
	}
	
	public void removeHeartbeatListener(IHeartbeatListener listener) {
		heartbeatListeners.remove(listener);
	}
	
	public void notifyNodeCollapsed(HostInfo hostInfo) {
		for(IHeartbeatListener listener : heartbeatListeners) listener.notifyNodeCollapsed(hostInfo);
	}
	
	private HostInfo getNextHost() {
		return hosts.get(nextHost.getAndIncrement() % hosts.size());
	}
	
	@Override
	public void heartbeatReceived(ITransportMessage data) {
		log.log(Level.FINE, "Received heartbeat response:" + data.toString());
		heartBeatReceived = true;
	}
	
	@Override
	public void run() {
		while(!shutDown) {
			heartBeatReceived = false;
			int tryCount = 0;
			try {
				HostInfo h = getNextHost();
				while(!heartBeatReceived && tryCount < 10) {
					HeartbeatMessage msg = new HeartbeatMessage(h.getHost(), h.getHost(), h.getPort(), TransportParams.CLIENT_LISTEN_PORT);
					transportSender.sendHeartbeatMessage(msg);
					tryCount++;
					Thread.sleep(1500);
				}
				if(!heartBeatReceived) {
					log.log(Level.WARNING, "Node: " + h.getHost().getHostAddress() + ":" + h.getPort() +
						" is not responding to heartbeat requests. Removing it from nodes list..");
					if(hosts.size() > 1) {
						hosts.remove(h);
						notifyNodeCollapsed(h);
					}
				}
			} catch(InterruptedException ie) {
				log.log(Level.FINE, "HeartbetSender interrupted..");
				shutDown = true;
			}
		}
		log.log(Level.INFO, "Heartbeat sender is quitting..");
	}
}
