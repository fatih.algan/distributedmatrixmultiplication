package com.linovi.client;

public interface INodeManagerObserver {

	public void notifyResultComplete(int nodeIndex, int[][] result); 
}
