package com.linovi.common.mock;

import com.linovi.client.IHeartbeatListener;
import com.linovi.common.transport.HostInfo;

public class MockHeartbeatListener implements IHeartbeatListener {

	private HostInfo collapsedHost = null;
	
	@Override
	public void notifyNodeCollapsed(HostInfo hostInfo) {
		this.collapsedHost = hostInfo;		
	}

	public HostInfo getCollapsedHost() {
		return collapsedHost;
	}
}
