package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class PartialResultAckData extends PartialMatrixAckData {

	private final int partitionId;
		
	public static final int HEADER_LENGTH = 20;
	
	public PartialResultAckData(int nodeIndex, int partitionId, int sequence, InetAddress host, 
		int port, int listenPort) {
		super(nodeIndex, sequence, host, port, listenPort);
		this.partitionId = partitionId;
	}
	
	public int getPartitionId() {
		return partitionId;
	}

	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH);
		buf.putInt(9);
		buf.putInt(getNodeIndex());
		buf.putInt(partitionId);
		buf.putInt(getSequence());
		buf.putInt(getListenPort());
		return buf.array();
	}
	
	protected static PartialResultAckData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 9) throw new RuntimeException("This message is not a PartialResultAckData message: " + cmd);
		int nodeIndex = buf.getInt();
		int partitionId = buf.getInt();
		int sequence = buf.getInt();
		int listenPort = buf.getInt();
		PartialResultAckData data = new PartialResultAckData(nodeIndex, partitionId, sequence, 
			host, port, listenPort);
		return data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PartialResultAck from [" + getHost().getHostAddress() + ":" + getListenPort() + "] ");
		builder.append("[node:" + getNodeIndex() + " - partition:" + getPartitionId() + " - seq:" +  getSequence() + " - ");
		builder.append("]");
		return builder.toString();
	}
}
