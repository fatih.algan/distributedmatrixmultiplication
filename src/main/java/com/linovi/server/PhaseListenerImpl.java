package com.linovi.server;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportDataProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.InitAckData;
import com.linovi.common.transport.InitData;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialResultAckData;
import com.linovi.common.transport.PartialResultData;
import com.linovi.common.transport.PartialSecondMatrixAckData;
import com.linovi.common.transport.PartialSecondMatrixData;
import com.linovi.common.transport.RequestOperationResultData;
import com.linovi.common.transport.TerminateNodeData;
import com.linovi.common.transport.TransportParams;

public class PhaseListenerImpl implements IPhaseListener, ITransportDataProcessor {

	private static final Logger log = Logger.getGlobal();
	private Map<Integer, NodeMultiplier> nodeMultipliers = new HashMap<>();
	
	private ITransportDataSender transportSender = null;
	private HostInfo localHostInfo = null;
		
	int maxNumElementsInPacket = (TransportParams.MAX_PACKET_SIZE - PartialResultData.HEADER_LENGTH) / 4;
	
	
	public PhaseListenerImpl(HostInfo localHostInfo, ITransportDataSender transportSender) {
		super();
		this.transportSender = transportSender;
		this.localHostInfo = localHostInfo;
	}
	
	public NodeMultiplier getNodeMultiplier(int nodeIndex) {
		return nodeMultipliers.get(nodeIndex);
	}
	
	@Override
	public void dataReceived(ITransportDataMessage msg) {
		log.log(Level.FINE, msg.toString());
		if(msg instanceof InitData) {
			InitData data = (InitData)msg;
			notifyInitCompleted(data);
		} else if(msg instanceof PartialSecondMatrixData) {
			PartialSecondMatrixData data = (PartialSecondMatrixData)msg;
			notifySecondMatrixPartReceived(data);
		} else if(msg instanceof PartialFirstMatrixData) {
			PartialFirstMatrixData data = (PartialFirstMatrixData)msg;
			notifyFirstMatrixPartReceived(data);
		} else if(msg instanceof RequestOperationResultData) {
			RequestOperationResultData data = (RequestOperationResultData)msg;
			notifyRequestOperationResultReceived(data);
		} else if(msg instanceof TerminateNodeData) {
			notifyTerminateNode((TerminateNodeData)msg);
		} else if(msg instanceof PartialResultAckData) {
			notifyPartialResultReceived((PartialResultAckData)msg);
		}
	}
	
	@Override
	public void notifyInitCompleted(InitData msg) {
		try {
			if(getNodeMultiplier(msg.getNodeIndex()) == null) {
				NodeMultiplier node = new NodeMultiplier(localHostInfo, msg.getNodeIndex(), 
					msg.getFirstMatrixColumnLength(), msg.getSecondMatrixRowLength(), 
					msg.getSecondMatrixColumnLength(), transportSender);
				nodeMultipliers.put(msg.getNodeIndex(), node);
			}
			InitAckData resp = new InitAckData(msg.getNodeIndex(), msg.getHost(),  
				msg.getListenPort(), localHostInfo.getPort());
			transportSender.sendTransportDataMessage(resp);
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error during initialization:" + e.getMessage());
		}
		
	}

	@Override
	public void notifySecondMatrixPartReceived(PartialSecondMatrixData msg) {
		try {
			NodeMultiplier node = getNodeMultiplier(msg.getNodeIndex());
			int rowIndex = msg.getRowIndex();
			int rowOffset = msg.getRowOffset();
			int[] partialMatrix = msg.getRowElements();
			node.writeSecondMatrixElements(rowIndex, rowOffset, partialMatrix);
			log.log(Level.FINE, "Recieved second matrix part:" + msg.toString());
			PartialSecondMatrixAckData response = new PartialSecondMatrixAckData(msg.getNodeIndex(), msg.getSequence(), msg.getHost(), 
			msg.getListenPort(), localHostInfo.getPort());
			transportSender.sendTransportDataMessage(response);
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error while writing second matrix data:" + e.getMessage());
		}
		
	}
	
	@Override
	public void notifyFirstMatrixPartReceived(PartialFirstMatrixData msg) {
		try {
			NodeMultiplier nodeMultiplier = getNodeMultiplier(msg.getNodeIndex());
			PartialFirstMatrixAckData response = nodeMultiplier.distributeToPartition(msg);
			transportSender.sendTransportDataMessage(response);
		} catch(Exception e) {
			log.log(Level.SEVERE, "Exception is distributing first matrix part to Node Multiplier:" + e.getMessage());
		}
	}

	@Override
	public void notifyRequestOperationResultReceived(RequestOperationResultData msg) {
		try {
			NodeMultiplier nodeMultiplier = getNodeMultiplier(msg.getNodeIndex());
			nodeMultiplier.requestPartitionResult(msg);
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error while calculating partition result:" + e.getMessage());
		}
	}
	
	public void shutDown() {
		Set<Integer> nms = nodeMultipliers.keySet();
		for(Integer i : nms) {
			NodeMultiplier nm = nodeMultipliers.get(i);
			nm.shutDown();
		}
		nodeMultipliers.clear();
	}

	@Override
	public void notifyTerminateNode(TerminateNodeData msg) {
		NodeMultiplier multiplier = nodeMultipliers.remove(msg.getNodeIndex());
		if(multiplier != null) {
			log.log(Level.INFO, "Removing and shutting down node multiplier: " + msg.getNodeIndex());
			multiplier.shutDown(); 
		}
	}

	@Override
	public void notifyPartialResultReceived(PartialResultAckData msg) {
		NodeMultiplier multiplier = nodeMultipliers.get(msg.getNodeIndex());
		if(multiplier != null) {
			multiplier.sequenceReceivedAck(msg.getPartitionId(), msg.getSequence());
		}
	}

}
