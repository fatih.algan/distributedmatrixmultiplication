package com.linovi.common.transport;

import com.linovi.client.IHeartbeatListener;

public class MockHeartbeatProcessor implements IHeartbeatProcessor {

	private boolean hearbeatReceived = false;
	
	@Override
	public void heartbeatReceived(ITransportMessage data) {
		this.hearbeatReceived = true;		
	}

	public boolean isHeartbeatReceived() {
		return hearbeatReceived;
	}

	@Override
	public void addHeartbeatListener(IHeartbeatListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeHeartbeatListener(IHeartbeatListener listener) {
		// TODO Auto-generated method stub
		
	}
	
}
