package com.linovi.server;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.client.MatrixManager;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialResultData;

public class PartitionMultiplier implements Runnable {

	private static final Logger log = Logger.getGlobal();
	
	private final int nodeId;
	
	private final HostInfo localHostInfo;
	private final HostInfo remoteHostInfo;
		
	private AtomicBoolean[] sequences;
	private ITransportDataSender transportDataSender;
	
	private final int partitionId;
	private final int[][] secondMatrix;
	private volatile int[][] firstMatrix;
	private volatile int[][] resultMatrix;
	
	private MatrixManager matrixManager;
	
	private AtomicBoolean calculating = new AtomicBoolean(false);
	
	private volatile int transferredSequencesCount = 0;
		
	public PartitionMultiplier(HostInfo remoteHostInfo, HostInfo localHostInfo, 
		ITransportDataSender transportSender, int nodeId, int partitionId, int[][] secondMatrix, 
		int firstMatrixRowLength, int firstMatrixColumnLength) {
		this.nodeId = nodeId;
		this.localHostInfo = localHostInfo;
		this.remoteHostInfo = remoteHostInfo;
		this.partitionId = partitionId;
		this.secondMatrix = secondMatrix;
		this.firstMatrix = new int[firstMatrixRowLength][firstMatrixColumnLength];
		this.resultMatrix = new int[firstMatrixRowLength][secondMatrix[0].length];
		this.matrixManager = new MatrixManager(resultMatrix);
		this.transportDataSender = transportSender;
		sequences = new AtomicBoolean[matrixManager.getTotalPackets()];
		for(int i = 0; i <sequences.length; i++) sequences[i] = new AtomicBoolean(false);
	}
	
	public int[][] getResultMatrix() {
		return resultMatrix;
	}

	public PartialFirstMatrixAckData writeFirstMatrixRowData(PartialFirstMatrixData msg) {
		int rowIndex = msg.getRowIndex();
		int rowOffset = msg.getRowOffset();
		int[] partialMatrix = msg.getRowElements();
		for(int i = 0; i < partialMatrix.length; i++) {
			firstMatrix[rowIndex][rowOffset] = partialMatrix[i];
			rowOffset = rowOffset + 1;
			if(rowOffset == firstMatrix[0].length) {
				rowOffset = 0;
				rowIndex++;
			}
		}
		log.log(Level.FINE, "Recieved second matrix part:" + msg.toString());
		PartialFirstMatrixAckData response = new PartialFirstMatrixAckData(msg.getNodeIndex(), 
			msg.getPartitionNo(), msg.getSequence(), msg.getHost(), msg.getListenPort(), 
			localHostInfo.getPort());
		return response;
	}
	
	public void sequenceReceivedAck(int sequenceNo) {
		sequences[sequenceNo].set(true);
		transferredSequencesCount++;
	}

	@Override
	public void run() {
		try {
			calculating.set(true);
			IMultiplierEngine engine = new MultiplierEngine(firstMatrix, secondMatrix, resultMatrix);
			log.log(Level.INFO, "Starting multiplication of node:" + nodeId + " - partition:" + partitionId);
			engine.multiply();
			log.log(Level.INFO, "Multiplication result node:" + nodeId + " - partition:" + partitionId + " complete");
			boolean keepRunning = true;
			boolean sequencesComplete = false;
			log.log(Level.INFO, "Checking whether result partition has been completely distributed.");
			while(!sequencesComplete && keepRunning) {
				sequencesComplete = true;
				try {
					for(int i = 0; i < matrixManager.getTotalPackets(); i++) {
						if(distributeSequence(i)) sequencesComplete = false;
						if(i % 5000 == 0) Thread.sleep(500);
					}
					log.log(Level.INFO, "Distributed %" + getCompletedSequencesPercentage() + " of result");
					Thread.sleep(2000);
				} catch(InterruptedException ie) {
					keepRunning = false;
				}
			}
		} catch(Exception e) {
			log.log(Level.INFO, "Error in partition multiplier: " + e.getMessage());
			e.printStackTrace();
		}
		log.log(Level.INFO, "Parition Multiplier node - " + nodeId + " partition - " + partitionId + " is terminating..");
		calculating.set(false);
	}
	
	private boolean distributeSequence(int sequenceNo) throws InterruptedException {
		int[] partialMatrix = matrixManager.getPartialRowOfSequence(sequenceNo);
		if(!sequences[sequenceNo].get()){
			PartialResultData msg = new PartialResultData(nodeId, partitionId, sequenceNo, 
				matrixManager.getTotalPackets(), partialMatrix, remoteHostInfo.getHost(), remoteHostInfo.getPort(),
				localHostInfo.getPort());
			log.log(Level.FINE, "Sending partial result: " + msg.toString());
			transportDataSender.sendTransportDataMessage(msg);
			return true;
		}
		return false;
	}


	public boolean isCalculating() {
		return calculating.get();
	}
	
	public int getCompletedSequencesPercentage() {
		if(sequences == null) return 0;
		return (transferredSequencesCount / sequences.length) * 100;
	}
	
}
