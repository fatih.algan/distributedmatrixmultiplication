package com.linovi.common.transport;


public interface ITransportDataMessage extends ITransportMessage {

	public int getSequence();
	public int getNodeIndex();
	
}
