package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public abstract class PartialMatrixAckData implements ITransportDataMessage {

	private final int sequence;
	
	private final int nodeIndex;
	private final InetAddress host;
	private final int port;
	private final int listenPort;
	
	public static final int HEADER_LENGTH = 16;
	
	public PartialMatrixAckData(int nodeIndex, int sequence, InetAddress host, int port, 
		int listenPort) {
		this.nodeIndex = nodeIndex;
		this.sequence = sequence;
		this.host = host;
		this.port = port;
		this.listenPort = listenPort;
	}
	
	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}
	
	public int getSequence() {
		return sequence;
	}
	
	public int getNodeIndex() {
		return nodeIndex;
	}
	
	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH);
		buf.putInt(4);
		buf.putInt(nodeIndex);
		buf.putInt(sequence);
		buf.putInt(listenPort);
		return buf.array();
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RowAck from [" + host.getHostAddress() + ":" + listenPort + "] ");
		builder.append("[node:" + nodeIndex + " - seq:" +  sequence + " - ");
		builder.append("]");
		return builder.toString();
	}
}
