package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

public abstract class PartialMatrixData implements ITransportDataMessage {

	private final int sequence;
	
	private final int[] rowElements;
	private final int nodeIndex;
	private final int rowIndex;
	private final int rowOffset;
	private final InetAddress host;
	private final int port;
	private final int listenPort;
	
	public static final int HEADER_LENGTH = 24;
	
	public PartialMatrixData(int nodeIndex, int sequence, int rowIndex, int rowOffset, 
		int[] rowElements, InetAddress host, int port, int listenPort) {
		this.nodeIndex = nodeIndex;
		this.sequence = sequence;
		this.rowElements = rowElements;
		this.rowIndex = rowIndex;
		this.rowOffset = rowOffset;
		this.host = host;
		this.port = port;
		this.listenPort = listenPort;
	}
	
	public int[] getRowElements() {
		return rowElements;
	}

	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}
	
	public int getSequence() {
		return sequence;
	}
	
	public int getNodeIndex() {
		return nodeIndex;
	}
	
	public int getRowIndex() {
		return rowIndex;
	}

	public int getRowOffset() {
		return rowOffset;
	}

	public byte[] encode() {
		int dataLength = (rowElements.length) * 4;
		ByteBuffer buf = ByteBuffer.allocate(dataLength + HEADER_LENGTH);
		buf.putInt(3);
		buf.putInt(nodeIndex);
		buf.putInt(sequence);
		buf.putInt(listenPort);
		buf.putInt(rowIndex);
		buf.putInt(rowOffset);
		for(int i = 0; i < rowElements.length; i++) {
			buf.putInt(rowElements[i]);
		}
		return buf.array();
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Row from [" + host.getHostAddress() + ":" + listenPort + "] ");
		builder.append("[node:" + nodeIndex + " - seq:" +  sequence + " - ");
		builder.append("{");
		builder.append(Arrays.toString(rowElements));
		builder.append("} ");
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + listenPort;
		result = prime * result + nodeIndex;
		result = prime * result + port;
		result = prime * result + rowIndex;
		result = prime * result + rowOffset;
		result = prime * result + sequence;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartialMatrixData other = (PartialMatrixData) obj;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (listenPort != other.listenPort)
			return false;
		if (nodeIndex != other.nodeIndex)
			return false;
		if (port != other.port)
			return false;
		if (rowIndex != other.rowIndex)
			return false;
		if (rowOffset != other.rowOffset)
			return false;
		if (sequence != other.sequence)
			return false;
		return true;
	}
	
	
}
