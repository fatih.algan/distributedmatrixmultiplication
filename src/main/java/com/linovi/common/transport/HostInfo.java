package com.linovi.common.transport;

import java.net.InetAddress;
import java.util.StringTokenizer;

public class HostInfo {

	private InetAddress host;
	private int port;
	
	public HostInfo(String host, String port) {
		resolveHost(host, port);
	}
	
	public HostInfo(String host, int port) {
		resolveHost(host, String.valueOf(port));
	}
	
	public HostInfo(InetAddress host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public HostInfo(String info) {
		StringTokenizer tok = new StringTokenizer(info, ":");
		String host = tok.nextToken();
		String port = tok.nextToken();
		resolveHost(host, port);
	}
	
	private void resolveHost(String host, String port) {
		try {
			this.host = InetAddress.getByName(host);
			this.port = Integer.parseInt(port);
		} catch(Exception e) {
			throw new IllegalArgumentException("Could not resolve host info: " + host + ":" + port);
		}
	}

	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + port;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HostInfo other = (HostInfo) obj;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
	
	public String toString() {
		return "[" + host.getHostAddress() + ":" + port + "]";
	}
}
