package com.linovi.common.transport;

import java.net.InetAddress;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class HeartbeatMessageTest {

	int cmd = 3;
	int listenPort = 80;
	byte[] encoded = null;
	int port = 110;
	
	HeartbeatMessage msg = null;
	
	@Before
	public void setUp() {
		try {
			msg = new HeartbeatMessage(InetAddress.getLocalHost(), InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			HeartbeatMessage decoded = (HeartbeatMessage)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getReplyHost(), InetAddress.getLocalHost());
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host");
		}
	}
}
