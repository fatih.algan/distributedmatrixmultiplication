package com.linovi.server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class MultiplierEngine extends RecursiveAction implements IMultiplierEngine {

	private static final long serialVersionUID = -8003222748941234193L;
	
	private final int[][] firstMatrix, secondMatrix, result;
	private final int row;
	
	public MultiplierEngine(int[][] first, int[][] second, int[][] result) {
		this(first, second, result, -1);
	}
	
	private MultiplierEngine(int[][] first, int[][] second, int[][] result, int row) {
		if(first[0].length != second.length) throw new IllegalArgumentException("rows/columns mismatch");
		this.firstMatrix = first;
		this.secondMatrix = second;
		this.result = result;
		this.row = row;
	}
	
	@Override
	public void multiply() {
		ForkJoinPool pool = new ForkJoinPool();
	    pool.invoke(this);
	    pool.shutdown();
	}
	
	@Override
	public void compute() {
		if(row == -1) {
			List<MultiplierEngine> tasks = new ArrayList<>();
			for(int row = 0; row < firstMatrix.length; row++) {
				tasks.add(new MultiplierEngine(firstMatrix, secondMatrix, result, row));
			}
			invokeAll(tasks);
		} else {
			multiplyRowByColumn(firstMatrix, secondMatrix, result, row);
		}
	}
	
	public void multiplyRowByColumn(int[][] a, int[][] b, int[][] c, int row) {
		for(int j = 0; j < b[0].length; j++) {
			for(int k = 0; k < a[0].length; k++) {
				c[row][j] = c[row][j] + (a[row][k] * b[k][j]);
			}
		}
	}

	public int[][] getResult() {
		return result;
	}
	
}
