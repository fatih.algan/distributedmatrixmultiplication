package com.linovi.client;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockHeartbeatListener;
import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.ITransportMessage;
import com.linovi.common.transport.MockTransportDataReceiver;

import junit.framework.Assert;

public class HeartbeatManagerTest {

	private HeartbeatManager manager;
	
	MockTransportDataSender transportSender;
	MockHeartbeatListener listener = new MockHeartbeatListener();
	
	HostInfo host1 = new HostInfo("192.168.1.82", 8080);
	HostInfo host2 = new HostInfo("192.168.1.80", 8080);
	
	@Before
	public void setUp() {
		
		transportSender = new MockTransportDataSender();
		List<HostInfo> hosts = new ArrayList<>();
		hosts.add(host1);
		hosts.add(host2);
		manager = new HeartbeatManager(transportSender, hosts);
		manager.addHeartbeatListener(listener);
	}
	
	@Test
	public void testAddedHeartbeatMessages() {
		Thread t = new Thread(manager);
		t.start();
		try{
			Thread.sleep(7000);
		} catch(Exception e) {
			
		}
		ITransportMessage msg = transportSender.pollFromHeartbeat();
		Assert.assertEquals(msg.getHost(), host1.getHost());
		Assert.assertEquals(msg.getPort(), host1.getPort());
	}
	
	@Test
	public void testAddHeartbeatListener() {
		manager.notifyNodeCollapsed(host2);
		Assert.assertEquals(host2, listener.getCollapsedHost());
		Assert.assertNotSame(host1, listener.getCollapsedHost());
	}
}
