package com.linovi.threaded;

import org.junit.Before;
import org.junit.Test;

import com.linovi.client.PartitionInfo;
import com.linovi.client.SendPartialMatrixManager;
import com.linovi.common.mock.MockPartialResultObserver;
import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialResultData;

import junit.framework.Assert;

public class TestSendPartialMatrixTest {

	HostInfo host;
	MockTransportDataSender transportSender;
	PartitionInfo partitionInfo = new PartitionInfo(host, 0, 0, 1, 7);
	int[][] wholeMatrix = new int[][]{{8, 0, 8, 6, 3, 3}, {6, 2, 7, 4, 6, 6}, {8, 9, 5, 6, 0, 0}, {8, 2, 9, 1, 7, 5}, {5, 2, 3, 9, 1, 5}};
	int[][] resultPartition = new int[][] {{104, 85, 141, 78, 152, 158, 142}, {128, 103, 158, 80, 152, 175, 133}};
	SendPartialMatrixManager manager = null;
	MockPartialResultObserver resultObserver;
	
	@Before
	public void setUp() {
		host = new HostInfo("192.168.0.16", 1131);
		transportSender = new MockTransportDataSender();
		resultObserver = new MockPartialResultObserver();
		manager = new SendPartialMatrixManager(host, transportSender, 0, partitionInfo, wholeMatrix);
	}
	
	@Test
	public void testDistributePartition() {
		Thread t = new Thread(manager);
		t.start();
		try { Thread.sleep(1000); } catch(Exception e) {}
		transportSender.shutDown();
		PartialFirstMatrixAckData ack = new PartialFirstMatrixAckData(0, 0, 0, host.getHost(), host.getPort(), 1141);
		manager.dataReceived(ack);
		transportSender.shutDown();
		PartialResultData res = new PartialResultData(0, 0, 0, 4, new int[] {104, 85, 141, 78}, host.getHost(), host.getPort(), 1141);
		manager.dataReceived(res);
		res = new PartialResultData(0, 0, 1, 4, new int[] {152, 158, 142, 128}, host.getHost(), host.getPort(), 1141);
		manager.dataReceived(res);
		res = new PartialResultData(0, 0, 2, 4, new int[] {103, 158, 80, 152}, host.getHost() , host.getPort(), 1141);
		manager.dataReceived(res);
		res = new PartialResultData(0, 0, 3, 4, new int[] {175, 133}, host.getHost(), host.getPort(), 1141);
		manager.dataReceived(res);
		try { Thread.sleep(500);} catch(Exception e) {};
		Assert.assertEquals(manager.getCompletedPercentage(), 100);
	}
}
