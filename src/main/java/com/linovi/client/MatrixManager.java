package com.linovi.client;

import com.linovi.common.transport.PartialMatrixData;
import com.linovi.common.transport.TransportParams;

public class MatrixManager {
	
	private volatile int[][] matrix = null;
	
	private int totalPackets;
	
	private final int maxNumElementsInPacket = (TransportParams.MAX_PACKET_SIZE - PartialMatrixData.HEADER_LENGTH) / 4;
	
	public MatrixManager(int[][] matrix) {
		this.matrix = matrix;
		int totalElements = matrix.length * matrix[0].length;
		totalPackets = (totalElements / maxNumElementsInPacket);
		if(totalElements % maxNumElementsInPacket != 0) totalPackets++;
	}
	
	public int getBeginningRowIndexOfSequence(int sequenceNo) {
		return (sequenceNo * maxNumElementsInPacket) / matrix[0].length;  
	}
	
	public int getBeginningRowOffsetOfSequence(int sequenceNo) {
		return (sequenceNo * maxNumElementsInPacket) %  matrix[0].length;
	}
	
	public int[] getPartialRowOfSequence(int sequenceNo) {
		int rowIndex = getBeginningRowIndexOfSequence(sequenceNo);
		int offSet = getBeginningRowOffsetOfSequence(sequenceNo);
		int elementCount = 0;
		if(sequenceNo == totalPackets - 1) {
			int remainder = (matrix.length * matrix[0].length) % maxNumElementsInPacket; 
			if(remainder == 0) elementCount = maxNumElementsInPacket;
			else elementCount = remainder;
		}
		else elementCount = maxNumElementsInPacket;
		int[] partialRow = new int[elementCount];
		for(int i = 0; i < elementCount; i++) {
			partialRow[i] = matrix[rowIndex][offSet];
			offSet = offSet + 1;
			if(offSet == matrix[0].length) {
				offSet = 0;
				rowIndex++;
			}
		}
		return partialRow;
	}

	public int[][] getMatrix() {
		return matrix;
	}

	public int getTotalPackets() {
		return totalPackets;
	}

	public int getMaxNumElementsInPacket() {
		return maxNumElementsInPacket;
	}
}
