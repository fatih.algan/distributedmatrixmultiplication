package com.linovi.common.transport;

import java.net.InetAddress;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

public class PartialFirstMatrixDataTest {

	int sequence = 0;
	
	int[] rowElements = new int[]{1, 3, 5, 7};
	int nodeIndex = 1;
	int partitionNo = 5;
	int rowIndex = 2;
	int rowOffset = 5;
	int port = 80;
	int listenPort = 110;
	
	byte[] encoded = null;
	PartialFirstMatrixData msg;
	
	@Before
	public void setUp() {
		try {
			msg = new PartialFirstMatrixData(nodeIndex, partitionNo, sequence, rowIndex, rowOffset,
				4, rowElements, InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			PartialFirstMatrixData decoded = (PartialFirstMatrixData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getRowIndex(), msg.getRowIndex());
			Assert.assertEquals(decoded.getRowOffset(), msg.getRowOffset());
			Assert.assertEquals(decoded.getTotalRowLength(), msg.getTotalRowLength());
			Assert.assertArrayEquals(decoded.getRowElements(), msg.getRowElements());
		} catch(Exception e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
