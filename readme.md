If running under Linux setting _JAVA_OPTS -Djava.net.preferIPv4Stack=true might be needed.
Set MAVEN_OPTS Xmx envorinment variable if memory is not sufficient for very large matrixes

For Servers:
mvn exec:java -Dexec.mainClass=com.linovi.server.ServerNode -Dexec.args="1111 192.168.1.28” 
mvn exec:java -Dexec.mainClass=com.linovi.server.ServerNode -Dexec.args="1121 192.168.1.28” 
mvn exec:java -Dexec.mainClass=com.linovi.server.ServerNode -Dexec.args="1131 192.168.1.28”

For Client:
mvn exec:java -Dexec.mainClass=com.linovi.client.ClientNode -Dexec.args=“~/development/EclipseWorkspaces/matrix/hosts.txt 192.168.1.28” 

Note:example hosts.txt file is included.

--------------------------------------------------------------------------------------------------

The program will randomly generate two matrix pairs as node-1 and node-2 and will dump the results
of each.

Each node will be divided into partitions same as the server number. For example if there are 3 servers each node will be divided into partition-0, partition-1 and partition-2

The client checks the servers via regular heart beat requests. If any of the servers fail,
the currently assigned partitions on the node will be re-distributed to other
servers as new partitions. The existing scheduled partition will be removed.

If the network is under very high load, servers might fail to send heart beat to client under some
circumstances and working server nodes might be removed from hosts list.
I tried to remedy this case by increasing the sleep times of other threads and putting more
retry counts to HeartbeatManager. But the issue might rise again in a different network.