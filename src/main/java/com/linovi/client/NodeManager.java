package com.linovi.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.ITransportListener;
import com.linovi.common.transport.TerminateNodeData;
import com.linovi.common.transport.TransportParams;
import com.linovi.common.transport.UDPTransportListener;

public class NodeManager implements Runnable, IHeartbeatListener, IPartialResultObserver {

	private static final Logger log = Logger.getGlobal();
	
	private ITransportListener transportDataListener;	
	private ITransportDataSender transportDataSender;
	
	private List<HostInfo> hosts;
	private final int nodeIndex;
	private final int[][] firstMatrix;
	private final int[][] secondMatrix;
	private volatile int[][] resultMatrix = null;
	
	private volatile boolean shutdown = false;
	
	private AtomicInteger nextPartitionId = new AtomicInteger(0);
	
	private InitManager initManager;
	private SendWholeMatrixManager secondMatrixManager;
	private Map<PartitionInfo, SendPartialMatrixManager> firstMatrixManagers = new ConcurrentHashMap<>();
	
	private INodeManagerObserver nodeManagerObserver;
	
	public NodeManager(int nodeIndex, int[][] firstMatrix, int[][] secondMatrix,
		ITransportDataSender transportDataSender, ITransportListener transportDataListener, 
		List<HostInfo> hosts, INodeManagerObserver nodeManagerObserver) {
		this.nodeIndex = nodeIndex;
		this.firstMatrix = firstMatrix;
		this.secondMatrix = secondMatrix;
		this.transportDataSender = transportDataSender;
		this.transportDataListener = transportDataListener;
		this.nodeManagerObserver = nodeManagerObserver;
		this.resultMatrix = new int[firstMatrix.length][secondMatrix[0].length];
		this.hosts = hosts;
		initManager = new InitManager(hosts, transportDataSender, nodeIndex, 
			firstMatrix.length, firstMatrix[0].length, secondMatrix.length, secondMatrix[0].length);
		secondMatrixManager = new SendWholeMatrixManager(hosts, transportDataSender, nodeIndex, secondMatrix);
		//Heartbeat listener create partitions dan sonra attch et.
		createPartitions(firstMatrix, 0);
		
	}
	
	public int getNodeIndex() {
		return nodeIndex;
	}
	
	public int[][] getResultMatrix() {
		return resultMatrix;
	}
	
	public int[][] getFirstMatrix() {
		return firstMatrix;
	}

	public int[][] getSecondMatrix() {
		return secondMatrix;
	}
	
	public Map<PartitionInfo, SendPartialMatrixManager> getFirstMatrixManagers() {
		return firstMatrixManagers;
	}

	public void setFirstMatrixManagers(Map<PartitionInfo, SendPartialMatrixManager> firstMatrixManagers) {
		this.firstMatrixManagers = firstMatrixManagers;
	}

	@Override
	public void run() {
		try {
			transportDataListener.addTransportDataListener(initManager);
			Thread t = new Thread(initManager);
			t.start();
			t.join();
			//At this point init ack is received, can start distributing the second matrix.
			log.log(Level.INFO, "Initialization handshake with server nodes is completed..");
			transportDataListener.removeTransportDataListener(initManager);
			
			transportDataListener.addTransportDataListener(secondMatrixManager);
			t = new Thread(secondMatrixManager);
			t.start();
			t.join();
			//At this point second matrix is completely distributed to the servers.
			log.log(Level.INFO, "Second matrix has fully been distributed to servers..");
			transportDataListener.removeTransportDataListener(secondMatrixManager);
			firstMatrixManagers.forEach((k, v) -> {
				Thread mm = new Thread(v);
				mm.setDaemon(true);
				mm.start();
			});
			while(!shutdown) {
				Thread.sleep(3000);
				firstMatrixManagers.keySet().stream().forEach(pi -> notifyPartitionComplete(pi));
				log.log(Level.INFO, "Node " + nodeIndex + " %" + getCompletedPercentage() + " complete..");
			}
			Thread.sleep(1000);
			nodeManagerObserver.notifyResultComplete(nodeIndex, resultMatrix);
		} catch(InterruptedException ie) {
			log.log(Level.INFO, "Matrix Multiplier is interrupted and exiting..");
		}
	}
	
	@Override
	public void notifyNodeCollapsed(HostInfo hostInfo) {
		log.warning("Host " + hostInfo.toString() + " is not responding. Will distribute the incomplete partitions to other nodes..");
		hosts.remove(hostInfo);
		Optional<PartitionInfo> pi = 
			firstMatrixManagers.keySet().stream().filter(p -> p.getHostInfo().equals(hostInfo))
			.findFirst();
		if(!pi.isPresent()) return;
		PartitionInfo partitionToReschedule = pi.get();
		SendPartialMatrixManager toRemove = firstMatrixManagers.get(partitionToReschedule);
		SendPartialMatrixManager managerToStop = firstMatrixManagers.get(partitionToReschedule);
		managerToStop.shutDown();
		transportDataListener.removeTransportDataListener(toRemove);
		int[][] partitionMatrix = new int[partitionToReschedule.getEndRowIndex() - partitionToReschedule.getStartRowIndex() + 1][];
		System.arraycopy(firstMatrix, partitionToReschedule.getStartRowIndex(), 
			partitionMatrix, 0, partitionMatrix.length);
		List<SendPartialMatrixManager> partialManagers = createPartitions(partitionMatrix, partitionToReschedule.getStartRowIndex());
		partialManagers.forEach(mm -> {
			Thread t = new Thread(mm);
			t.setDaemon(true);
			t.start();
		});
		firstMatrixManagers.remove(partitionToReschedule);
	}
	
	private List<SendPartialMatrixManager> createPartitions(int[][] matrix, int parentStartIndex) {
		int totalPartitions = hosts.size();
		if(hosts.size() > matrix.length) totalPartitions = matrix.length;
		int totalNumRows = matrix.length;
		int partitionRows = Math.round(((float)totalNumRows / (float)totalPartitions));
		List<SendPartialMatrixManager> partialManagers = new ArrayList<>();
		for(int i = 0; i < totalPartitions; i++) {
			int startRowIndex = i * partitionRows;
			if(startRowIndex > matrix.length) startRowIndex = ((i - 1) * partitionRows) + 1;
			int endRowIndex = 0;
			endRowIndex = startRowIndex + partitionRows - 1;
			if(endRowIndex > matrix.length - 1) endRowIndex = matrix.length - 1;
			if((i == totalPartitions - 1) && endRowIndex < matrix.length - 1) endRowIndex = matrix.length - 1; 
			int[][] partitionMatrix = new int[endRowIndex - startRowIndex + 1][];
			System.arraycopy(matrix, startRowIndex, partitionMatrix, 0, partitionMatrix.length);
			PartitionInfo partition = new PartitionInfo(hosts.get(i), nextPartitionId.getAndIncrement(), 
				startRowIndex + parentStartIndex, endRowIndex + parentStartIndex, secondMatrix[0].length);
			SendPartialMatrixManager firstMatrixManager = new SendPartialMatrixManager(hosts.get(i), 
				transportDataSender, nodeIndex, partition, partitionMatrix);
			firstMatrixManagers.put(partition, firstMatrixManager);
			transportDataListener.addTransportDataListener(firstMatrixManager);
			partialManagers.add(firstMatrixManager);
		}
		return partialManagers;
	}

	@Override
	public void notifyPartitionComplete(PartitionInfo partitionInfo) {
		try {
			if(isAllParitionsComplete()) {
				sendTerminateNodeDataMessage();
				mergePartitions();
				shutDown();
				log.log(Level.INFO, "Result for node " + nodeIndex + " has been completed..");
			}
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error while cleaning up Node Manager after partitions completion:" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public boolean isAllParitionsComplete() {
		return firstMatrixManagers.keySet().stream().allMatch(p -> p.isAllSequencesComplete()); 
	}
	
	private void mergePartitions() {
		TreeSet<PartitionInfo> partitions = new TreeSet<>(firstMatrixManagers.keySet());
		partitions.forEach(pi -> {
			System.arraycopy(pi.getResultMatrix(), 0, resultMatrix, 
					pi.getStartRowIndex(), pi.getResultMatrix().length);
		});
	}

	public void shutDown() {
		firstMatrixManagers.forEach((k, v) -> {
			transportDataListener.removeTransportDataListener(v);
			firstMatrixManagers.remove(k);
			v.shutDown();
		});
		shutdown = true;
	}
	
	public int getCompletedPercentage() {
		Optional<Integer> sum = firstMatrixManagers.entrySet().stream().
				map(v -> v.getValue().getCompletedPercentage()).reduce(Integer::sum);
		if(sum.isPresent()) return sum.get();
		return 100;
	}
	
	public void sendTerminateNodeDataMessage() {
		hosts.forEach(hi -> {
			TerminateNodeData msg = new TerminateNodeData(nodeIndex, hi.getHost(), hi.getPort(), 
				TransportParams.CLIENT_LISTEN_PORT);
			transportDataSender.sendTransportDataMessage(msg);
		});
	}
}
