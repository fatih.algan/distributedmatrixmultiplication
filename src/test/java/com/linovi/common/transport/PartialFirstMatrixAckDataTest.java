package com.linovi.common.transport;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PartialFirstMatrixAckDataTest {

	int sequence = 0;
	
	int nodeIndex = 1;
	int partitionId = 3;
	InetAddress host;
	int port = 80;
	int listenPort = 110;
	
	byte[] encoded = null;
	PartialFirstMatrixAckData msg;
	
	@Before
	public void setUp() {
		try {
			msg = new PartialFirstMatrixAckData(nodeIndex, sequence, partitionId, InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			PartialFirstMatrixAckData decoded = (PartialFirstMatrixAckData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getNodeIndex(), msg.getNodeIndex());
			Assert.assertEquals(decoded.getSequence(), msg.getSequence());
			Assert.assertEquals(decoded.getPartitionId(), msg.getPartitionId());
		} catch(UnknownHostException e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
