package com.linovi.common.transport;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPTransportListener implements Runnable, ITransportListener {

	private static final Logger log = Logger.getGlobal();
	
	private final int port;
	private DatagramSocket socket = null;
	private volatile boolean shutDown = false;
	private InetAddress bindAddress = null;
		
	private List<ITransportDataProcessor> dataListeners = new CopyOnWriteArrayList<>();
	private List<IHeartbeatProcessor> heartbeatListeners = new CopyOnWriteArrayList<>();
	
	public UDPTransportListener(InetAddress host, int port) {
		this.bindAddress = host;
		this.port = port;
	}
	
	public UDPTransportListener(int port) {
		this.port = port;
		try {
			this.bindAddress = InetAddress.getLocalHost();
		} catch(UnknownHostException he) {
			throw new IllegalArgumentException("Could not resolve localhost bind address: " + he.getMessage());
		}
		
	}
	
	public UDPTransportListener(String strBindAddress, int port) {
		try {
			bindAddress = InetAddress.getByName(strBindAddress);
		} catch(UnknownHostException he) {
			throw new IllegalArgumentException("Could not resolve bind address: " + strBindAddress);
		}
		this.port = port;
	}
	
	public void addTransportDataListener(ITransportDataProcessor listener) {
		dataListeners.add(listener);
	}
	
	public void removeTransportDataListener(ITransportDataProcessor listener) {
		dataListeners.remove(listener);
	}
	
	public void addHeartbeatListener(IHeartbeatProcessor listener) {
		heartbeatListeners.add(listener);
	}
	
	public void removeHeartbeatListener(IHeartbeatProcessor listener) {
		heartbeatListeners.remove(listener);
	}
	
	@Override
	public void run() {
		try {
			socket = new DatagramSocket(port, bindAddress);
			log.log(Level.INFO, "Server listening on - " + socket.getLocalAddress().getHostAddress() + ":" + socket.getLocalPort());
			while(!shutDown) {
				DatagramPacket request = new DatagramPacket(new byte[TransportParams.MAX_PACKET_SIZE], TransportParams.MAX_PACKET_SIZE);
				socket.receive(request);
				byte[] message = readMessage(request);
				log.log(Level.FINE, "Received packet length: " + message.length);
				ITransportMessage result = decodeMessage(message, request.getAddress(), request.getPort());
				log.log(Level.FINE, "Received message: " + result.toString());
				try {	
					notifyListeners(result);
				} catch(InterruptedException ie) {
					shutDown = true;
				}
			}
		} catch(IOException | RuntimeException re) {
			socket.close();
		}
		if(!socket.isClosed()) socket.close();
		log.log(Level.INFO, "Shutting down UDPTransportListener...");
	}
	
	
	public void shutDown() {
		shutDown = true;
		socket.close();
	}

	private void notifyListeners(ITransportMessage data) throws InterruptedException {
		if(data instanceof HeartbeatMessage) {
			for(IHeartbeatProcessor listener : heartbeatListeners) {
				listener.heartbeatReceived(data);
			}
		} else if(data instanceof ITransportDataMessage) {
			for(ITransportDataProcessor listener : dataListeners) {
				listener.dataReceived((ITransportDataMessage)data);
			}
		} 
	}
	
	private byte[] readMessage(DatagramPacket packet) {
		byte[] result = new byte[packet.getLength()];
		System.arraycopy(packet.getData(), 0, result, 0, result.length);
		return result;
	}
	
	private ITransportMessage decodeMessage(byte[] message, InetAddress host, int port) {
		return ITransportMessage.decode(message, host, port);
	}
	
}
