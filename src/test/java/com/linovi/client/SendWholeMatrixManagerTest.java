package com.linovi.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.mock.TransportMessageSequenceComparator;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportMessage;
import com.linovi.common.transport.PartialSecondMatrixData;

public class SendWholeMatrixManagerTest extends AbstractClientManagerTest {

	SendWholeMatrixManager manager;
	
	int[][] matrix = new int[][]{{91, 125, 51}, {117, 128, 58}, {105, 133, 44}, {83, 102, 55}};
	int[] expectedResult = new int[] {91, 125, 51, 117, 128, 58, 105, 133, 44, 83, 102, 55};
	int[] resultBuffer = new int[12];
	
	@Before
	public void setUp() {
		host1 = new HostInfo("localhost", 80);
		hosts.add(host1);
		transportSender = new MockTransportDataSender();
		manager = new SendWholeMatrixManager(hosts, transportSender, 0, matrix);
	}
	
	@Test
	public void testDistributeSequence() {
		Thread t = new Thread(manager);
		t.start();
		try {
			Thread.sleep(200);
		} catch(Exception e) {
			
		}
		t.interrupt();
		PartialSecondMatrixData data = null;
		ITransportDataMessage msg = null;
		Set<ITransportDataMessage> queued = new HashSet<>();
		while((msg = transportSender.pollFromTransport()) != null) {
			queued.add(msg);
		}
		ArrayList<ITransportDataMessage> queuedWithOrder = new ArrayList<>(queued);
		Collections.sort(queuedWithOrder, new TransportMessageSequenceComparator());
		int index = 0;
		for(ITransportDataMessage item : queuedWithOrder) {
			data = (PartialSecondMatrixData)item;
			System.arraycopy(data.getRowElements(), 0, resultBuffer, index, data.getRowElements().length);
			index = index + data.getRowElements().length;
		}
		Assert.assertArrayEquals(expectedResult, resultBuffer);
	}
	
	@Test
	public void testAckReceived() {
		
	}
	
}
