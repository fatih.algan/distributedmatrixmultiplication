package com.linovi.common.mock;

import com.linovi.client.INodeManagerObserver;

public class MockNodeManagerObserver implements INodeManagerObserver {

	private boolean resultComplete = false;
	
	@Override
	public void notifyResultComplete(int nodeIndex, int[][] result) {
		resultComplete = true;		
	}

	public boolean isResultComplete() {
		return resultComplete;
	}
}
