package com.linovi.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockNodeManagerObserver;
import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.mock.MockTransportListener;
import com.linovi.common.transport.HostInfo;

import junit.framework.Assert;

public class NodeManagerTest {

	MockTransportListener transportListener;
	MockTransportDataSender transportSender;
	List<HostInfo> hosts;
	MockNodeManagerObserver nodeObserver;
	NodeManager manager;
	
	HostInfo host1 = new HostInfo("192.168.0.16", 1131);
	HostInfo host2 = new HostInfo("192.168.0.16", 1121);
	
	int[][] firstMatrix = new int[][]{{8, 0, 8, 6, 3, 3}, {6, 2, 7, 4, 6, 6}, {8, 9, 5, 6, 0, 0}, {8, 2, 9, 1, 7, 5}, {5, 2, 3, 9, 1, 5}};
	int[][] secondMatrix = new int[][] {{0, 2, 5, 2, 9, 5, 4}, {6, 6, 1, 6, 3, 9, 8}, {4, 3, 4, 4, 4, 5, 7}, {7, 4, 5, 4, 4, 8, 8}, {3, 4, 7, 0, 5, 3, 1}, {7, 3, 6, 2, 3, 7, 1}};
	
	@Before
	public void setUp() {
		hosts = new ArrayList<>();
		hosts.add(host1);
		hosts.add(host2);
		transportListener = new MockTransportListener();
		transportSender = new MockTransportDataSender();
		manager = new NodeManager(0, firstMatrix, secondMatrix, transportSender, transportListener, hosts, nodeObserver);
	}
	
	@Test
	public void testCreatePartitions() {
		Map<PartitionInfo, SendPartialMatrixManager> partialManagers = manager.getFirstMatrixManagers();
		Set<PartitionInfo> partitions = partialManagers.keySet();
		TreeSet<PartitionInfo> sorted = new TreeSet<>(partitions);
		Assert.assertEquals(sorted.size(), 2);
		PartitionInfo pi = sorted.pollFirst();
		Assert.assertEquals(pi.getStartRowIndex(), 0);
		Assert.assertEquals(pi.getEndRowIndex(), 2);
		pi = sorted.pollFirst();
		Assert.assertEquals(pi.getStartRowIndex(), 3);
		Assert.assertEquals(pi.getEndRowIndex(), 4);
	}
	
	@Test
	public void testNotifyNodeCollapsed() {
		manager.notifyNodeCollapsed(host1);
		Map<PartitionInfo, SendPartialMatrixManager> partialManagers = manager.getFirstMatrixManagers();
		Set<PartitionInfo> partitions = partialManagers.keySet();
		TreeSet<PartitionInfo> sorted = new TreeSet<>(partitions);
		Assert.assertEquals(sorted.size(), 2);
		PartitionInfo pi = sorted.pollFirst();
		Assert.assertEquals(pi.getStartRowIndex(), 0);
		Assert.assertEquals(pi.getEndRowIndex(), 2);
		Assert.assertEquals(pi.getPartitionId(), 2);
		Assert.assertEquals(pi.getHostInfo(), host2);
		pi = sorted.pollFirst();
		Assert.assertEquals(pi.getStartRowIndex(), 3);
		Assert.assertEquals(pi.getEndRowIndex(), 4);
		Assert.assertEquals(pi.getPartitionId(), 1);
		Assert.assertEquals(pi.getHostInfo(), host2);
	}
}
