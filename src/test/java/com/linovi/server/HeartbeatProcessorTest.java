package com.linovi.server;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HeartbeatMessage;
import com.linovi.common.transport.HostInfo;

import junit.framework.Assert;
import junit.framework.TestCase;

public class HeartbeatProcessorTest extends TestCase {

	MockTransportDataSender transportSender;
	HeartbeatProcessor heartbeatProcessor;
	
	HostInfo host1;
	
	@Before
	public void setUp() {
		try {
			host1 = new HostInfo(InetAddress.getLocalHost(), 110);
		} catch(UnknownHostException e) {
			throw new RuntimeException("Unknown host");
		}
		transportSender = new MockTransportDataSender();
		heartbeatProcessor = new HeartbeatProcessor(transportSender);
	}
	
	@Test
	public void testDataReceived() {
		HeartbeatMessage msg = new HeartbeatMessage(host1.getHost(), host1.getHost(), 80, 110);
		try {
			heartbeatProcessor.heartbeatReceived(msg);
		} catch(InterruptedException ie) {
			
		}
		Assert.assertEquals(msg.getHost(), host1.getHost());
		Assert.assertEquals(msg.getListenPort(), host1.getPort());
	}
}
