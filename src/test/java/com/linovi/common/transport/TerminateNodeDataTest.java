package com.linovi.common.transport;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TerminateNodeDataTest {

	int nodeIndex = 0;
	InetAddress host;
	int port = 80;
	int listenPort = 110;
	
	byte[] encoded = null;
	TerminateNodeData msg;
	
	@Before
	public void setUp() {
		try {
			 msg = new TerminateNodeData(nodeIndex, InetAddress.getLocalHost(), port, listenPort);
		} catch(Exception e) {
			throw new RuntimeException("Unresolved host..");
		}
	}
	
	@Test
	public void testEncodeDecode() {
		try {
			encoded = msg.encode();
			TerminateNodeData decoded = (TerminateNodeData)ITransportMessage.decode(encoded, InetAddress.getLocalHost(), port);
			Assert.assertEquals(decoded.getListenPort(), msg.getListenPort());
			Assert.assertEquals(decoded.getHost(), msg.getHost());
			Assert.assertEquals(decoded.getPort(), msg.getPort());
			Assert.assertEquals(decoded.getClass(), msg.getClass());
			Assert.assertEquals(decoded.getNodeIndex(), msg.getNodeIndex());
		} catch(UnknownHostException e) {
			throw new RuntimeException("Unresolved Host");
		}
	}
}
