package com.linovi.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialResultData;
import com.linovi.common.transport.RequestOperationResultData;
import com.linovi.common.transport.TransportParams;

public class NodeMultiplier {

	private static final Logger log = Logger.getGlobal();
	int maxNumElementsInPacket = (TransportParams.MAX_PACKET_SIZE - PartialResultData.HEADER_LENGTH) / 4;
	
	private HostInfo localHostInfo = null;
	private ITransportDataSender transportSender;
	
	private final int nodeId;
	private volatile int[][] secondMatrix;
	private final int firstMatrixColumnLength;
			
	private AtomicBoolean running = new AtomicBoolean(false);
	
	private Map<Integer, PartitionMultiplier> partitionMultipliers = new HashMap<>();
	private List<Thread> partitionMultiplierThreads = new ArrayList<>();
		
	public NodeMultiplier(HostInfo localHostInfo, int nodeId, int firstMatrixColumnLength, 
		int secondMatrixRowLength, int secondMatrixColumnLength,
		ITransportDataSender transportSender) {
		this.nodeId = nodeId;
		this.localHostInfo = localHostInfo;
		this.secondMatrix = new int[secondMatrixRowLength][secondMatrixColumnLength];
		for(int i = 0; i < secondMatrix.length; i++) 
			for(int k = 0; k < secondMatrix[0].length; k++) secondMatrix[i][k] = -1;
		this.firstMatrixColumnLength = firstMatrixColumnLength;
		this.transportSender = transportSender;
	}
	
	public void writeSecondMatrixElements(int rowIndex, int rowOffset, int[] partialMatrix) {
		for(int i = 0; i < partialMatrix.length; i++) {
			secondMatrix[rowIndex][rowOffset] = partialMatrix[i];
			rowOffset = rowOffset + 1;
			if(rowOffset == secondMatrix[0].length) {
				rowOffset = 0;
				rowIndex++;
			}
		}
	}
	
	public boolean isRunning() {
		return running.compareAndSet(false, true);
	}
	
	public PartialFirstMatrixAckData distributeToPartition(PartialFirstMatrixData data) {
		int partitionId = data.getPartitionNo();
		PartitionMultiplier pMultiplier = null;
		HostInfo remoteHostInfo = new HostInfo(data.getHost(), data.getListenPort());
		if(!partitionMultipliers.containsKey(partitionId)) {
			pMultiplier = new PartitionMultiplier(remoteHostInfo, localHostInfo, transportSender, nodeId, 
				data.getPartitionNo(), secondMatrix, data.getTotalRowLength(), firstMatrixColumnLength);
			partitionMultipliers.put(partitionId, pMultiplier);
		} else{
			pMultiplier = partitionMultipliers.get(partitionId);
		}
		PartialFirstMatrixAckData ack = pMultiplier.writeFirstMatrixRowData(data);
		return ack;
	}
	
	public PartitionMultiplier getPartitionMultiplier(int partitionId) {
		return partitionMultipliers.get(partitionId);
	}
	
	public void requestPartitionResult(RequestOperationResultData data) {
		Integer partitionId = data.getPartitionId();
		PartitionMultiplier multiplier = partitionMultipliers.get(partitionId);
		if(multiplier.isCalculating()) {
			log.log(Level.INFO, "Node multiplier already processing " + nodeId + " - partition:" + partitionId);
			return;
		}
		Thread t = new Thread(multiplier);
		partitionMultiplierThreads.add(t);
		t.start();
	}
	
	public void sequenceReceivedAck(int partitionId, int sequenceNo) {
		PartitionMultiplier multiplier = partitionMultipliers.get(partitionId);
		if(multiplier != null) multiplier.sequenceReceivedAck(sequenceNo);
	}
	
	public void shutDown() {
		for(Thread t : partitionMultiplierThreads) {
			t.interrupt();
		}
	}
}
