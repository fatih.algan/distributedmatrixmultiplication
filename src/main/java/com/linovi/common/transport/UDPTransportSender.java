package com.linovi.common.transport;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPTransportSender implements ITransportDataSender, Runnable {
	
	private static final Logger log = Logger.getGlobal();
	
	private LinkedBlockingQueue<ITransportMessage> transportDataQueue = new LinkedBlockingQueue<>(5000);
	private LinkedBlockingQueue<ITransportMessage> transportHeartbeatQueue = new LinkedBlockingQueue<>();
	private volatile boolean shutDown = false;
	
	DatagramSocket socket = null;
	
	public UDPTransportSender() {
		super();
	}
	
	@Override
	public void sendTransportDataMessage(ITransportDataMessage data) {
		try {
			transportDataQueue.put(data);
		} catch(InterruptedException ie) {
			shutDown = true;
		}
	}
	
	@Override
	public void sendHeartbeatMessage(ITransportMessage data) {
		transportHeartbeatQueue.add(data);
	}
	
	private void send(ITransportMessage data, boolean useListenPort) throws IOException {
		byte[] message = data.encode();
		log.log(Level.FINE, "Encoded packet length: " + message.length);
		if(message == null || message.length == 0) return;
		DatagramPacket request = null;
		if(useListenPort) request = new DatagramPacket(message, message.length, data.getHost(), data.getListenPort());
		else request = new DatagramPacket(message, message.length, data.getHost(), data.getPort());
		log.log(Level.FINE, "Sending message: " + data.toString());
		socket.send(request);
	}

	@Override
	public void run() {
		try {
			socket = new DatagramSocket();
			while(!shutDown) {
				ITransportMessage heartbeat = transportHeartbeatQueue.poll();
				if(heartbeat != null) send(heartbeat, false);
				ITransportMessage data = transportDataQueue.poll();
				if(data != null) send(data, false);
			} 
		} catch(IOException oe) {
			shutDown = true;
		} finally {
			socket.close();
		}
		log.log(Level.INFO, "UDP Transport sender is quitting..");
	}
	
	public void shutDown() {
		shutDown = true;
	}
}
