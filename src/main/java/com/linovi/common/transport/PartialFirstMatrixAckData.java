package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class PartialFirstMatrixAckData extends PartialMatrixAckData {

	private final int partitionId;
	
	public static final int HEADER_LENGTH = 20;
	
	public PartialFirstMatrixAckData(int nodeIndex, int partitionId, int sequence, InetAddress host, 
		int port, int listenPort) {
		super(nodeIndex, sequence, host, port, listenPort);
		this.partitionId = partitionId;
	}

	public int getPartitionId() {
		return partitionId;
	}
	
	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH);
		buf.putInt(6);
		buf.putInt(getNodeIndex());
		buf.putInt(getPartitionId());
		buf.putInt(getSequence());
		buf.putInt(getListenPort());
		return buf.array();
	}
	
	protected static PartialFirstMatrixAckData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 6) throw new RuntimeException("This message is not a PartialFirstMatrixAckData message: " + cmd);
		int nodeIndex = buf.getInt();
		int partitionId = buf.getInt();
		int sequence = buf.getInt();
		int listenPort = buf.getInt();
		PartialFirstMatrixAckData data = new PartialFirstMatrixAckData(nodeIndex, partitionId, sequence, 
			host, port, listenPort);
		return data;
	}
	
}
