package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class RequestOperationResultData implements ITransportDataMessage {

	private final int sequence;
	private final int nodeIndex;
	private final int partitionId;
	private final InetAddress host;
	private final int port;
	private final int listenPort;
	
	public static final int HEADER_LENGTH = 16;
	
	public RequestOperationResultData(int nodeIndex, int partitionId, InetAddress host, int port,
		int listenPort) {
		this.sequence = 0;
		this.partitionId = partitionId;
		this.nodeIndex = nodeIndex;
		this.host = host;
		this.port = port;
		this.listenPort = listenPort;
	}

	public int getSequence() {
		return sequence;
	}

	public int getNodeIndex() {
		return nodeIndex;
	}

	public int getPartitionId() {
		return partitionId;
	}

	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}
	
	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH);
		buf.putInt(7);
		buf.putInt(nodeIndex);
		buf.putInt(partitionId);
		buf.putInt(listenPort);
		return buf.array();
	}
	
	protected static RequestOperationResultData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 7) throw new RuntimeException("This message is not a RequestOperationResult message: " + cmd);
		int nodeIndex = buf.getInt();
		int partitionId = buf.getInt();
		int listenPort = buf.getInt();
		RequestOperationResultData data = new RequestOperationResultData(nodeIndex, partitionId, host, port, listenPort);
		return data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequestOperationResult from [" + host.getHostAddress() + ":" + listenPort + "] ");
		builder.append("[node:" + nodeIndex);
		builder.append("]");
		return builder.toString();
	}
}
