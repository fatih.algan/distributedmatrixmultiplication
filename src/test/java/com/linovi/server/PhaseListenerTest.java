package com.linovi.server;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.InitAckData;
import com.linovi.common.transport.InitData;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialSecondMatrixAckData;
import com.linovi.common.transport.PartialSecondMatrixData;

import junit.framework.Assert;

public class PhaseListenerTest {

	PhaseListenerImpl phaseListener;
	MockTransportDataSender transportSender;
	
	HostInfo localHostInfo;
	HostInfo host;
	
	int[][] firstMatrix = new int[][]{{8, 0, 8, 6, 3, 3}, {6, 2, 7, 4, 6, 6}, {8, 9, 5, 6, 0, 0}, {8, 2, 9, 1, 7, 5}, {5, 2, 3, 9, 1, 5}};
	int[][] secondMatrix = new int[][] {{0, 2, 5, 2, 9, 5, 4}, {6, 6, 1, 6, 3, 9, 8}, {4, 3, 4, 4, 4, 5, 7}, {7, 4, 5, 4, 4, 8, 8}, {3, 4, 7, 0, 5, 3, 1}, {7, 3, 6, 2, 3, 7, 1}};
	
	InitData initData = null;
	PartialSecondMatrixData secondMatrixData;
	PartialFirstMatrixData firstMatrixData;
	
	@Before
	public void setUp() {
		host = new HostInfo("192.168.0.16", 1141);
		localHostInfo = new HostInfo("192.168.0.12", 1131);
		transportSender = new MockTransportDataSender();
		phaseListener = new PhaseListenerImpl(localHostInfo, transportSender);
	}
	
	void init() {
		initData = new InitData(0, host.getHost(), host.getPort(), 1141, 5, 6, 6, 7);
		secondMatrixData = new PartialSecondMatrixData(0, 1, 0, 4, new int[] {9, 5, 4, 6}, host.getHost(), host.getPort(), 1141);
		firstMatrixData = new PartialFirstMatrixData(0, 1, 0, 0, 0, 7, new int[] {8, 0, 8, 6}, host.getHost(), host.getPort(), 1141);
	}
	
	@Test
	public void testNotifyInitCompleted() {
		init();
		phaseListener.notifyInitCompleted(initData);
		InitAckData initAck = (InitAckData)transportSender.pollFromTransport();
		Assert.assertEquals(initAck.getPort(), initData.getListenPort());
		Assert.assertEquals(initAck.getHost(), initData.getHost());
	}
	
	@Test
	public void testNotifySecondMatrixPartReceived() {
		init();
		phaseListener.notifyInitCompleted(initData);
		transportSender.pollFromTransport();
		phaseListener.notifySecondMatrixPartReceived(secondMatrixData);
		PartialSecondMatrixAckData ack = (PartialSecondMatrixAckData)transportSender.pollFromTransport();
		Assert.assertEquals(ack.getSequence(), secondMatrixData.getSequence());
		Assert.assertEquals(ack.getPort(), secondMatrixData.getPort());
	}
	
	@Test
	public void notifyFirstMatrixPartReceived() {
		init();
		phaseListener.notifyInitCompleted(initData);
		transportSender.pollFromTransport();
		phaseListener.notifySecondMatrixPartReceived(secondMatrixData);
		transportSender.pollFromTransport();
		phaseListener.notifyFirstMatrixPartReceived(firstMatrixData);
		PartialFirstMatrixAckData ack = (PartialFirstMatrixAckData)transportSender.pollFromTransport();
		Assert.assertEquals(ack.getSequence(), firstMatrixData.getSequence());
		Assert.assertEquals(ack.getPort(), firstMatrixData.getPort());
	}
	
	
}
