package com.linovi.common.transport;

public class MockTransportDataReceiver implements ITransportDataProcessor {

	private boolean dataReceived = false;
	
	@Override
	public void dataReceived(ITransportDataMessage data) {
		dataReceived = true;		
	}
	
	public boolean isDataReceived() {
		return dataReceived;
	}

}
