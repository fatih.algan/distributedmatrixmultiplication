package com.linovi.common.mock;

import java.util.ArrayList;
import java.util.List;

import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportDataProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.ITransportMessage;

public class MockTransportDataSender implements ITransportDataSender {

	private boolean addedToTransportQueue = false;
	
	private List<ITransportDataMessage> transportQueue = new ArrayList<>();
	private List<ITransportMessage> heartbeatQueue = new ArrayList<>();
	
	public boolean isDataSent() {
		return transportQueue.size() > 0;
	}
	
	public boolean isHeartbeatSent() {
		return heartbeatQueue.size() > 0;
	}
	
	@Override
	public void sendTransportDataMessage(ITransportDataMessage data) {
		transportQueue.add(data);
		
	}

	@Override
	public void sendHeartbeatMessage(ITransportMessage data) {
		heartbeatQueue.add(data);		
	}
	
	public ITransportDataMessage pollFromTransport() {
		if(transportQueue.size() == 0) return null;
		return transportQueue.remove(0);
	}
	
	public ITransportMessage pollFromHeartbeat() {
		if(heartbeatQueue.size() == 0) return null;
		return heartbeatQueue.remove(0);
	}

	@Override
	public void shutDown() {
		transportQueue.clear();
		heartbeatQueue.clear();
	}
	
}
