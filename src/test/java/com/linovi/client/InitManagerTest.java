package com.linovi.client;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.mock.MockTransportDataSender;
import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportMessage;
import com.linovi.common.transport.InitAckData;

import junit.framework.Assert;

public class InitManagerTest extends AbstractClientManagerTest {

	InitManager initManager;
	
	@Before
	public void setUp() {
		host1 = new HostInfo("localhost", 80);
		host2 = new HostInfo("192.168.0.12", 111);
		hosts.add(host1);
		hosts.add(host2);
		transportSender = new MockTransportDataSender();
		initManager = new InitManager(hosts, transportSender, 0, 5, 6, 6, 4);
	}
	
	@Test
	public void testSendInitMessages() {
		Thread t = new Thread(initManager);
		t.start();
		try {
			Thread.sleep(200);
		} catch(InterruptedException ie) {
			
		}
		ITransportMessage initMsg1 = transportSender.pollFromTransport();
		Assert.assertEquals(host1.getHost(), initMsg1.getHost());
		ITransportMessage initMsg2 = transportSender.pollFromTransport();
		Assert.assertEquals(host2.getHost(), initMsg2.getHost());
	}
	
	@Test
	public void testInitAckReceived() {
		InitAckData msg1 = new InitAckData(0, host1.getHost(), host1.getPort(), host1.getPort());
		InitAckData msg2 = new InitAckData(0, host2.getHost(), host2.getPort(), host2.getPort());
		initManager.dataReceived(msg1);
		initManager.dataReceived(msg2);
		Assert.assertTrue(initManager.isPhaseComplete());
	}
}
