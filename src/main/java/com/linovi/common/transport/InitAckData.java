package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class InitAckData implements ITransportDataMessage {

	private final int sequence;
	private final int nodeIndex;
	private final InetAddress host;
	private final int port;
	private final int listenPort;
	
	public static final int HEADER_LENGTH = 12;
	
	public InitAckData(int nodeIndex, InetAddress host, int port, int listenPort) {
		this.sequence = 0;
		this.nodeIndex = nodeIndex;
		this.host = host;
		this.port = port;
		this.listenPort = listenPort;
	}
	
	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}
	
	public int getSequence() {
		return sequence;
	}
	
	public int getNodeIndex() {
		return nodeIndex;
	}
	
	public byte[] encode() {
		ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH + 4);
		buf.putInt(2);
		buf.putInt(nodeIndex);
		buf.putInt(listenPort);
		return buf.array();
	}
	
	protected static InitAckData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 2) throw new RuntimeException("This message is not an InitAck message: " + cmd);
		int nodeIndex = buf.getInt();
		int listenPort = buf.getInt();
		InitAckData data = new InitAckData(nodeIndex, host, port, listenPort);
		return data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InitAck from [" + host.getHostAddress() + ":" + listenPort + "] ");
		builder.append("[node:" + nodeIndex);
		builder.append("]");
		return builder.toString();
	}
}
