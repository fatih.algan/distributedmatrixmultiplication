package com.linovi.server;

import com.linovi.common.transport.InitData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialMatrixData;
import com.linovi.common.transport.PartialResultAckData;
import com.linovi.common.transport.PartialSecondMatrixAckData;
import com.linovi.common.transport.PartialSecondMatrixData;
import com.linovi.common.transport.RequestOperationResultData;
import com.linovi.common.transport.TerminateNodeData;

public interface IPhaseListener {

	public void notifyInitCompleted(InitData msg);
	public void notifySecondMatrixPartReceived(PartialSecondMatrixData msg);
	public void notifyFirstMatrixPartReceived(PartialFirstMatrixData msg);
	public void notifyRequestOperationResultReceived(RequestOperationResultData msg);
	public void notifyPartialResultReceived(PartialResultAckData msg);
	public void notifyTerminateNode(TerminateNodeData msg);

}
