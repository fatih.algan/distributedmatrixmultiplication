package com.linovi.common.transport;

public interface ITransportDataProcessor {

	public void dataReceived(ITransportDataMessage msg);
}
