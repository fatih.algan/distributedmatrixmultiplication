package com.linovi.common.mock;

import java.util.Comparator;

import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportMessage;

public class TransportMessageSequenceComparator implements Comparator<ITransportDataMessage>{

	@Override
	public int compare(ITransportDataMessage o1, ITransportDataMessage o2) {
		return new Integer(o1.getSequence()).compareTo(o2.getSequence());
	}

	
}
