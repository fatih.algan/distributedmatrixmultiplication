package com.linovi.client;

public interface IPartialResultObserver {

	public void notifyPartitionComplete(PartitionInfo partitionInfo);	
}
