package com.linovi.client;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.ITransportDataMessage;
import com.linovi.common.transport.ITransportDataProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.PartialFirstMatrixAckData;
import com.linovi.common.transport.PartialFirstMatrixData;
import com.linovi.common.transport.PartialResultAckData;
import com.linovi.common.transport.PartialResultData;
import com.linovi.common.transport.RequestOperationResultData;
import com.linovi.common.transport.TransportParams;

public class SendPartialMatrixManager implements Runnable, ITransportDataProcessor {

	private static final Logger log = Logger.getGlobal();
	
	private ITransportDataSender transportSender;
	
	private final int nodeId;
	
	private PartitionInfo partitionInfo;
	
	private HostInfo host;
	
	private AtomicBoolean[] sequences;
	
	private volatile  boolean shutDown = false;
	
	private volatile boolean operationResultArriving = false;
	
	private volatile boolean partialResultComplete = false;
	
	private final MatrixManager matrixManager;
	
	
	public SendPartialMatrixManager(HostInfo host, ITransportDataSender transportSender, int nodeIndex,
		PartitionInfo partitionInfo, int[][] wholeMatrix) {
		this.nodeId = nodeIndex;
		this.transportSender = transportSender;
		this.matrixManager = new MatrixManager(wholeMatrix);
		this.host = host;
		this.partitionInfo = partitionInfo;
		sequences = new AtomicBoolean[matrixManager.getTotalPackets()];
		IntStream.range(0, matrixManager.getTotalPackets()).forEach(i -> sequences[i] = new AtomicBoolean(false));
		log.log(Level.FINE, "Total sequence count is: " + matrixManager.getTotalPackets());
	}
	
	public void shutDown() {
		shutDown = true;
	}

	@Override
	public void dataReceived(ITransportDataMessage msg) {
		log.log(Level.FINE, msg.toString());
		if(nodeId != msg.getNodeIndex()) return;
		if(msg instanceof PartialFirstMatrixAckData) {
			PartialFirstMatrixAckData data = (PartialFirstMatrixAckData)msg;
			if(data.getPartitionId() != partitionInfo.getPartitionId()) return;
			sequences[msg.getSequence()].set(true);
			
		} if(msg instanceof PartialResultData) {
			try {
				PartialResultData data = (PartialResultData)msg;
				if(data.getPartitionNo() != partitionInfo.getPartitionId()) return;
				operationResultArriving = true;
				log.log(Level.FINE, "Received result partition:" + data.toString());
				transportSender.sendTransportDataMessage(new PartialResultAckData(nodeId, data.getPartitionNo(), 
					data.getSequence(), host.getHost(), data.getListenPort(), TransportParams.CLIENT_LISTEN_PORT));
				writePartitionDataToResult(data);
			} catch(Exception e) {
				log.log(Level.SEVERE, "Error writing partial result: " + msg.toString());
			}
		} 
	}
	
	//When this thread completes all alive nodes will have received the complete second matrix
	//and sent their acks
	@Override
	public void run() {
		boolean sequencesComplete = false;
		while(!sequencesComplete && !shutDown) {
			log.log(Level.FINE, "Checking whether partition has been completely sent.");
			sequencesComplete = true;
			try {
				for(int i = 0; i < matrixManager.getTotalPackets(); i++) {
					if(distributeSequence(i)) sequencesComplete = false;
					if(i % 5000 == 0) Thread.sleep(500);
				}
				Thread.sleep(2000);
			} catch(InterruptedException ie) {
				shutDown = true;
			}
		}
		while(!operationResultArriving && !shutDown) {
			sendRequestOperationResultRequest();
			try {
				Thread.sleep(10000);
			} catch(InterruptedException ie) {
				shutDown = true;
			}
		}
		while(!partialResultComplete && !shutDown) {
			log.log(Level.FINE, "Checking whether result partitions have been completely received..");
			try {
				Thread.sleep(500);
			} catch(InterruptedException ie) {
				shutDown = true;
			}
			
		}
		log.log(Level.FINE, "PartialMatrixManager for node " + nodeId + " is terminating..");
	}
		
	private boolean distributeSequence(int sequenceNo) throws InterruptedException {
		try {
			int rowIndex = matrixManager.getBeginningRowIndexOfSequence(sequenceNo);
			int rowOffset = matrixManager.getBeginningRowOffsetOfSequence(sequenceNo);
			int[] partialMatrix = matrixManager.getPartialRowOfSequence(sequenceNo);
			if(!sequences[sequenceNo].get()) {
				PartialFirstMatrixData msg = new PartialFirstMatrixData(nodeId, partitionInfo.getPartitionId(), 
						sequenceNo, rowIndex, rowOffset, matrixManager.getMatrix().length, partialMatrix, host.getHost(), 
						host.getPort(), TransportParams.CLIENT_LISTEN_PORT);
				transportSender.sendTransportDataMessage(msg);
				return true;
			}
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error in writing partial write sequence:" + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	private void sendRequestOperationResultRequest() {
		RequestOperationResultData data = new RequestOperationResultData(nodeId, partitionInfo.getPartitionId(), 
			host.getHost(), host.getPort(), TransportParams.CLIENT_LISTEN_PORT);
		transportSender.sendTransportDataMessage(data);
	}
	
	private void writePartitionDataToResult(PartialResultData data) {
		log.log(Level.FINE, "Node - " + nodeId + " will write partition result: " + data.toString());
		try {
			partitionInfo.writePartialResult(data.getElements(), data.getSequence(), data.getTotalSequenceCount());
		} catch(Exception e) {
			log.log(Level.SEVERE, "Error while writing partition result of: " + data.toString());
			e.printStackTrace();
		}
	}
	
	public int getCompletedPercentage() {
		return partitionInfo.getCompletedSequencesPercentage();
	}
	
}
