package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class PartialResultData implements ITransportDataMessage {

	private final int sequence;
	private final int nodeIndex;
	private final int partitionNo;
	private final int totalSequenceCount;
	private final int[] elements;
	
	private final InetAddress host;
	private final int port;
	private final int listenPort;
	
	public static final int HEADER_LENGTH = 24;
	
	public PartialResultData(int nodeIndex, int partitionNo, int sequence, int totalSequenceCount, 
		int[] elements, InetAddress host, int port, int listenPort) {
		this.sequence = sequence;
		this.nodeIndex = nodeIndex;
		this.partitionNo = partitionNo;
		this.totalSequenceCount = totalSequenceCount;
		this.elements = elements;
		this.host = host;
		this.port = port;
		this.listenPort = listenPort;
	}
	
	public int getSequence() {
		return sequence;
	}

	public int getNodeIndex() {
		return nodeIndex;
	}

	public int getPartitionNo() {
		return partitionNo;
	}

	public int getTotalSequenceCount() {
		return totalSequenceCount;
	}

	public int[] getElements() {
		return elements;
	}

	public InetAddress getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getListenPort() {
		return listenPort;
	}

	public byte[] encode() {
		int dataLength = (elements.length) * 4;
		ByteBuffer buf = ByteBuffer.allocate(dataLength + HEADER_LENGTH);
		buf.putInt(8);
		buf.putInt(getNodeIndex());
		buf.putInt(getPartitionNo());
		buf.putInt(getSequence());
		buf.putInt(getTotalSequenceCount());
		buf.putInt(getListenPort());
		for(int i = 0; i < getElements().length; i++) {
			buf.putInt(getElements()[i]);
		}
		return buf.array();
	}
	
	protected static PartialResultData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 8) throw new RuntimeException("This message is not a PartialResultData message: " + cmd);
		int nodeIndex = buf.getInt();
		int partitionNo = buf.getInt(); 
		int sequence = buf.getInt();
		int totalSequenceCount = buf.getInt();
		int listenPort = buf.getInt();
		int[] elements = new int[(message.length - HEADER_LENGTH) / 4];
		for(int i = 0; i < elements.length; i++) {
			elements[i] = buf.getInt();
		}
		PartialResultData data = new PartialResultData(nodeIndex, partitionNo, sequence, 
			totalSequenceCount, elements, host, port, listenPort);
		return data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Partial result from [" + host.getHostAddress() + ":" + listenPort + "] ");
		builder.append("[node:" + nodeIndex + " - seq:" +  sequence + " - ");
		builder.append("{");
		builder.append(Arrays.toString(elements));
		builder.append("} ");
		builder.append("]");
		return builder.toString();
	}
}
