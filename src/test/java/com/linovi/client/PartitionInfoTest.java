package com.linovi.client;

import org.junit.Before;
import org.junit.Test;

import com.linovi.common.transport.HostInfo;

import org.junit.Assert;

public class PartitionInfoTest {

	int[][] matrix = new int[][]{{91, 125, 51}, {117, 128, 58}, {105, 133, 44}, {83, 102, 55}};
		
	HostInfo hostInfo;
	PartitionInfo info;

	@Before
	public void setUp() {
		hostInfo = new HostInfo("localhost", 80);
		info = new PartitionInfo(hostInfo, 0, 0, 3, 3);
	}
	
	@Test
	public void testWritePartitionResult() {
		info.writePartialResult(new int[] {91, 125, 51, 117, 128}, 0, 3);
		info.writePartialResult(new int[] {58, 105, 133, 44, 83}, 1, 3);
		info.writePartialResult(new int[] {102, 55}, 2, 3);
		Assert.assertArrayEquals(matrix, info.getResultMatrix());
	}
}
