package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public class PartialFirstMatrixData extends PartialMatrixData {

	private final int partitionNo;
	private final int totalRowLength;
	
	public static final int HEADER_LENGTH = 32;
	
	public PartialFirstMatrixData(int nodeIndex, int partitionNo, int sequence, int rowIndex, int rowOffset, 
		int totalRowLength, int[] rowElements,InetAddress host, int port, int listenPort) {
		super(nodeIndex, sequence, rowIndex, rowOffset, 
				rowElements, host, port, listenPort);
		this.partitionNo = partitionNo;
		this.totalRowLength = totalRowLength;
	}

	public int getPartitionNo() {
		return partitionNo;
	}
	
	public int getTotalRowLength() {
		return totalRowLength;
	}

	@Override
	public byte[] encode() {
		int dataLength = (getRowElements().length) * 4;
		ByteBuffer buf = ByteBuffer.allocate(dataLength + HEADER_LENGTH);
		buf.putInt(5);
		buf.putInt(getNodeIndex());
		buf.putInt(partitionNo);
		buf.putInt(getSequence());
		buf.putInt(getListenPort());
		buf.putInt(getRowIndex());
		buf.putInt(getRowOffset());
		buf.putInt(getTotalRowLength());
		for(int i = 0; i < getRowElements().length; i++) {
			buf.putInt(getRowElements()[i]);
		}
		return buf.array();
	}
	
	protected static PartialFirstMatrixData decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message);
		int cmd = buf.getInt();
		if(cmd != 5) throw new RuntimeException("This message is not a PartialFirstMatrixData message: " + cmd);
		int nodeIndex = buf.getInt();
		int partitionNo = buf.getInt(); 
		int sequence = buf.getInt();
		int listenPort = buf.getInt();
		int rowIndex = buf.getInt();
		int rowOffset = buf.getInt();
		int totalRowLength = buf.getInt();
		int[] rowElements = new int[(message.length - HEADER_LENGTH) / 4];
		for(int i = 0; i < rowElements.length; i++) {
			rowElements[i] = buf.getInt();
		}
		PartialFirstMatrixData data = new PartialFirstMatrixData(nodeIndex, partitionNo, sequence, 
			rowIndex, rowOffset, totalRowLength, rowElements, host, port, listenPort);
		return data;
	}
}
