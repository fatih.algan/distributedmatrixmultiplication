package com.linovi.client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.linovi.common.transport.HostInfo;
import com.linovi.common.transport.IHeartbeatProcessor;
import com.linovi.common.transport.ITransportDataSender;
import com.linovi.common.transport.TransportParams;
import com.linovi.common.transport.UDPTransportListener;
import com.linovi.common.transport.UDPTransportSender;

public class ClientNode implements INodeManagerObserver {

	private static final Logger log = Logger.getGlobal();
	
	private UDPTransportListener transportListener;
	private ITransportDataSender transportDataSender;
	private IHeartbeatProcessor heartbeatProcessor;
	private List<NodeManager> nodeManagers = new ArrayList<>();
	private CountDownLatch nodesLatch = null;
	private List<HostInfo> hostList = new CopyOnWriteArrayList<>();
	
	public ClientNode(List<String> hosts, String bindAddress) {
		if(bindAddress == null) transportListener = new UDPTransportListener(TransportParams.CLIENT_LISTEN_PORT);
		else transportListener = new UDPTransportListener(bindAddress, TransportParams.CLIENT_LISTEN_PORT);
		transportDataSender = new UDPTransportSender();
		hosts.stream().forEach(s -> {
			hostList.add(new HostInfo(s));
		});
		heartbeatProcessor = new HeartbeatManager(transportDataSender, hostList);
		transportListener.addHeartbeatListener(heartbeatProcessor);
	}
	
	public void addMatrixPair(int[][] firstMatrix, int[][] secondMatrix) {
		NodeManager node = new NodeManager(nodeManagers.size(), firstMatrix, secondMatrix, 
			transportDataSender, transportListener, hostList, this);
		heartbeatProcessor.addHeartbeatListener(node);
		nodeManagers.add(node);
	}
	
	
	public void execute() {
		List<Thread> nodeManagerT = new ArrayList<>();
		Thread transportSenderT = new Thread((UDPTransportSender)transportDataSender);
		transportSenderT.start();
		Thread transportListenerT = new Thread(transportListener);
		transportListenerT.start();
		Thread heartbeatT = new Thread((HeartbeatManager)heartbeatProcessor);
		heartbeatT.start();
		for(NodeManager nm : nodeManagers) {
			Thread t = new Thread(nm);
			nodeManagerT.add(t);
			t.start();
		}
		nodesLatch = new CountDownLatch(nodeManagers.size());
		try {
			nodesLatch.await();
		} catch(InterruptedException ie) { }
		transportDataSender.shutDown();
		transportListener.shutDown();
		transportListenerT.interrupt();
		heartbeatT.interrupt();
		for(Thread t : nodeManagerT) {
			t.interrupt();
		}
	}
	

	@Override
	public void notifyResultComplete(int nodeIndex, int[][] result) {
		try {
			Thread.sleep(3000);
		} catch(InterruptedException ie) {
			
		}
		NodeManager manager = nodeManagers.get(nodeIndex);
		log.log(Level.INFO, "Result for node " + nodeIndex + " has been completed:");
		dump(nodeIndex, manager.getResultMatrix());
		nodesLatch.countDown();
	}
	
	private int[][] generateMatrix(int rowSize, int columnSize) {
		Random rn = new Random();
		int[][] matrix = new int[rowSize][columnSize];
		for(int i = 0; i < rowSize; i++) {
			for(int k = 0;  k < columnSize; k++) {
				matrix[i][k] = rn.nextInt(10) + 1;			
			}
		}
		return matrix;
	}
	
	private static void dump(int nodeIndex, int[][] resultMatrix) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < resultMatrix.length; i++) {
			builder.append("\n");
			for(int k = 0; k < resultMatrix[0].length; k++) {
				builder.append(resultMatrix[i][k] + " ");
			}
		}
		builder.append("\n");
		System.out.println(builder.toString());
	}
	
	public static void main(String[] args) {
		LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.FINE);
		if(args.length == 0) throw new RuntimeException("Please specify hosts file path as first argument - Ex: c://hosts.txt");
		String localBindAddress = null;
		if(args.length == 2)  localBindAddress = args[1];
		String fileName = args[0];
		List<String> hosts = new ArrayList<>();
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			hosts = stream.collect(Collectors.toList());
		} catch (IOException e) {
			throw new RuntimeException("Error reading hosts file: " + e.getMessage());
		}
		ClientNode calculator = new ClientNode(hosts, localBindAddress);
		int[][] firstMatrix = calculator.generateMatrix(1127, 1375);
		int[][] secondMatrix = calculator.generateMatrix(1375, 1256);
		int[][] thirdMatrix = calculator.generateMatrix(1253, 1422);
		int[][] fourthMatrix = calculator.generateMatrix(1422, 1278);
		calculator.addMatrixPair(firstMatrix, secondMatrix);
		calculator.addMatrixPair(thirdMatrix, fourthMatrix);
		calculator.execute();
	}
	
}
