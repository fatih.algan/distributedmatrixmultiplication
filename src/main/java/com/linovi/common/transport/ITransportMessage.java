package com.linovi.common.transport;

import java.net.InetAddress;
import java.nio.ByteBuffer;

public interface ITransportMessage {
	
	public byte[] encode();
	public InetAddress getHost();
	public int getPort();
	public int getListenPort();
	
	public static ITransportMessage decode(byte[] message, InetAddress host, int port) {
		ByteBuffer buf = ByteBuffer.wrap(message, 0, 4);
		int cmd = buf.getInt();
		if(cmd == 0) return HeartbeatMessage.decode(message, host, port);
		if(cmd == 1) return InitData.decode(message, host, port);
		if(cmd == 2) return InitAckData.decode(message, host, port);
		if(cmd == 3) return PartialSecondMatrixData.decode(message, host, port);
		if(cmd == 4) return PartialSecondMatrixAckData.decode(message, host, port);
		if(cmd == 5) return PartialFirstMatrixData.decode(message, host, port);
		if(cmd == 6) return PartialFirstMatrixAckData.decode(message, host, port);
		if(cmd == 7) return RequestOperationResultData.decode(message, host, port);
		if(cmd == 8) return PartialResultData.decode(message, host, port);
		if(cmd == 9) return PartialResultAckData.decode(message, host, port);
		if(cmd == 10) return TerminateNodeData.decode(message, host, port);
		return null;
	};
}
